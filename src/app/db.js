var DBF = require('./dbFactory');
var Util = require('./util');
// 设置全局的`url`前缀
// 开发环境
if (__LOCAL__) {
    // var urlPrefixForMicroFlow = '//127.0.0.1:3000/';
    var urlPrefixForMicroFlow = "//api.mizlicai.com/";
}
if (__DEV__) {
    var urlPrefixForMicroFlow = '//121.43.148.191:8308/';
}
// 生产环境
if (__PRO__) {
    var urlPrefixForMicroFlow = "//api.mizlicai.com/";
}

DBF.set('urlPrefix', urlPrefixForMicroFlow);

DBF.set('defaultParsePesp',resp=>{
    if(resp.status === 'SUCCESS'){
        return resp.data || {};
    }else if(+resp.status === 21000){
        Util.goLogin();
    }
    return '';
});

DBF.create('Activity', {
    getList : {
        mockUrl   : 'mock/activities.json',
        url       : 'xplan/products',
        type      : 'GET'
    },
    getTop : {
        url       : 'xplan/products/type/banner',
        type      : 'GET'
    },
    getTopBanner:{
        url       : 'xplan/index/banner.json',
        type      : 'GET'
    },
    getSeckill : {
        url       : 'xplan/products/seckill',
        type      : 'GET'
    },
    getHot : {
        url       : 'xplan/products/hot',
        type      : 'GET'
    },
    getStock:{
        url       :'xplan/stock/{productId}',
        type      : 'GET'
    },
    getDirectBuy:{
        url       : 'xplan/directbuy',
        type      : 'POST'
    },
    getProducts:{
        url       :'xplan/products/{productId}',
        type      : 'GET'
    },
    getGroom:{
        url       :'xplan/index/recommend.json',
        type      :'get'
    }
});

DBF.create('Product', {
    getOneById : {
        mockUrl   : 'mock/one_product.json',
        url       : 'xplan/products/{productId}',
        type      : 'GET'
    },
    getOnePrice: {
        mockUrl   : 'mock/one_product_price.json',
        url       : 'xplan/products/{productId}/price',
        type      : 'POST'
    },
    getTheme:{
        url       : 'xplan/subject/{id}.json',
        type      : 'get'
    }
});

DBF.create('Order', {
    getOneById: {
        mockUrl   : 'mock/get_one_order.json',
        url       : 'xplan/norder/{orderId}',
        type      : 'GET'
    },
    createOrder: {
        url       : 'xplan/order',
        type      : 'POST'
    },
    confirmOrder: {
        url       : 'xplan/order/{orderId}',
        type      : 'POST'
    },
    getList: {
        mockUrl   : 'mock/order_list.json',
        url       : 'xplan/norder',
        type      : 'GET'
    },
    getCheckOrderLimit:{
        url       :'xplan/order/checkOrderLimit',
        type      :'GET'
    },
    getNorder:{
        url       :'xplan/norder/buy',
        type      :'post'  
    },
    deleteOrder:{
        url       :'xplan/norder/delete/{xOrderNo}',
        type      :'get'
    },
    rePay:{
        url       :'xplan/norder/pay',
        type      :'post'
    },
    loGistic:{
        url       :'xplan/logistics/{xOrderNo}',
        type      :'get'
    },
    newLoGistic:{
        url       :'xplan/logistics/last/{xOrderNo}',
        type      :'get'
    }
});

DBF.create('Address', {
    // getAddress : {
    //     url       : 'xplan/address',
    //     type      : 'GET'
    // },
    create: {
        url       : 'xplan/address',
        type      : 'POST'
    },
    update: {
        url       : 'xplan/address/{addressId}',
        type      : 'POST'
    },
    default:{
        url       : 'xplan/address/default',
        type      : 'GET'
    },
    info: {
        url       : 'xplan/address/info',
        type      : 'GET'
    },
});

//首页
DBF.create('Home', {
    getDetails : {
        url       : 'xplan/index/detail_v2',
        type      : 'GET'
    },
    getSecKill : {
        url       : 'xplan/activity/{type}',
        type      : 'GET'
    }
});


DBF.create('Goods', {
    getGoods:{
        url       : 'xplan/product/{id}.json',
        type      : 'get'
    },
    getProductAttr:{
        url       : 'xplan/product/attr',
        type      : 'post'
    },
    checkOrder:{
        url       :'xplan/norder/check',
        type      :'post'
    }
});

DBF.create('Dadian', {
    dot:{
        url       : 'api/alarmlogs.json',
        type      : 'post'
    }
});

module.exports = DBF.context;