var FastClick = require('fastclick');
if (__DEV__) {
    window.__loginUrl__ = '//mizlicai.chinacloudsites.cn/#/login?backTo=';
}
// 生产环境
if (__PRO__) {
    window.__loginUrl__ = "//h5.mizlicai.com/#/login?backTo=";
    if(location.protocol === 'http:'){
        location.protocol = 'https:';
    }
}

window.__event__ = {};

var React = require('react');
var ReactDOM = require('react-dom');
var ReactRouter = require("react-router").Route;
var Router = require('react-router').Router;
var hashHistory = require("react-router").hashHistory;

// 米享计划首页
var PageHome          = require('../pages/Home');
// 商品详情页
// var Goods             = require('../pages/Goods');
// 订单列表
var PageOrderList     = require('../pages/OrderList');
// 订单确认
// var PageOrderNew      = require('../pages/OrderNew')
// 订单详情
var PageOrderDetl     = require('../pages/OrderDetl');
// 米享计划攻略
// var PageXplan         = require('../pages/Xplan');
// 页面头
var Header            = require('../components/Header');
//物流详情
var Logistics         = require('../pages/Logistics');
//主题推荐
// var Theme             = require('../pages/Theme');
// var Util = require('./util');


$(()=> {
    FastClick.attach(document.body);
    var App = React.createClass({
        componentDidMount() {
            localStorage.homeTop = '';
            window.__IOSAPP__ = false;
            scroll(0,1);
        },

        getAppversion(){
            var app = navigator.appVersion;
            var ios = 'mizlicai_iOS';
            var and = 'mizlicai_Android';
            var ios_start = app.indexOf(ios);
            var and_start = app.indexOf(and);
            if(ios_start!==-1){//ios
                return gets(ios,ios_start);
            }else if(and_start!==-1){
                return gets(and,and_start);
            }

            function gets(version,start) {
                var end = app.substring(start).indexOf(')');
                return app.substring(start+version.length+1,start+end);
            }
        },

        render() {
            var version = this.getAppversion();

            var iphone = navigator.appVersion.indexOf('iPhone')!==-1;

            if(iphone?version>=2.8:version>282){
                window.__HEADER__ = false;
                if(iphone){
                    window.__IOSAPP__ = true;
                }
            }else{
                window.__HEADER__ = true;
            }

            if(navigator.userAgent.toLowerCase().match(/MicroMessenger/i)=="micromessenger"){
                window.__HEADER__ = false;
            }

            // alert(window.__HEADER__)
            window.__HEADER__ = false;


            return (
               <div ref='mt' className="xplan-container">
                    <Header />
                    <div className="body" style={{marginTop:(window.__HEADER__?'':0)}}>
                        {this.props.children}
                    </div>
               </div>
            )
        }
    });
    var routes = {
        path: '/',
        component: App,
        indexRoute: { component: PageHome },
        childRoutes: [
            { path: 'home',                component: PageHome },
            { path: 'order',               component: PageOrderList },
            // { path: 'orderNew',      component: PageOrderNew },
            { path: 'order_detl/:orderId', component: PageOrderDetl },
            // { path: 'xplan',               component: PageXplan },
            { path: 'logistics/:id',           component: Logistics},
            // { path: 'theme/:id',               component: Theme},
            // { path: 'goods/:id',               component: Goods},
            { path: '*',                component: PageHome}

        ]
    };
    ReactDOM.render(<Router history={hashHistory} routes={routes}/>, document.getElementById('mydom'));
});
