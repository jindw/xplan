module.exports = {

	getUrlParam: function(key) {
        var search = location.search;
        var arr = !search ? [] : location.search.substr(1).split('&');
        var param = {};
        for (var i=0, l=arr.length; i<l; i++) {
            var kv = arr[i].split('=');
            param[kv[0]] = kv[1];
        }
        return key ? (param[key] || '') : param;
    },

    getUserKey: function(redirectUrl,login){
    	var userKey = localStorage.getItem('auth');
        var hrefuk = location.hash.indexOf('userKey');
        if(hrefuk!==-1){//有
            var uk = location.hash.substring(hrefuk+8).indexOf('&');
            if(uk!==-1){
                userKey = location.hash.substring(hrefuk+8,hrefuk+8+uk);
            }else{
                userKey = location.hash.substring(hrefuk+8);
            }
        }

        if(userKey){
    		localStorage.setItem('auth', userKey);
    		return userKey;
    	}else{
            if(login === false)return false;
    		if(redirectUrl){
    			window.location.href=window.__loginUrl__+encodeURIComponent('//'+window.location.host+window.location.pathname+redirectUrl);
    		}else{
    			window.location.href=window.__loginUrl__+encodeURIComponent(window.location.href);
    		}
    	}
    },

    goLogin: function(){
    	window.location.href=window.__loginUrl__+encodeURIComponent(window.location.href);
    },

    getFenQi: function(key){
    	var fenqiMap = {
	        "1":'1周期',
	        "2":'半月期',
	        "3":'1月期',
	        "4":'2月期',
	        "5":'3月期',
	        "6":'6月期'
	    };
	    return fenqiMap[key];
    },

    customStyles: {
        content: {
            top                   : '50%',
            left                  : '50%',
            marginLeft            : '-100px',
            marginTop             : '-95px'
        }
    }
};