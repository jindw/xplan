var Action = require('./action');
var Reflux = require('reflux');
var Store = require('./store');
var React = require('react');
module.exports = {

	mixins: [Reflux.connect(Store)],

	Dot(dot,content){
        Action.addDot(dot,content||'');
	}
};