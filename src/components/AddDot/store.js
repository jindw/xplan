var Reflux = require('reflux');
var Action = require('./action');

var Store = Reflux.createStore({

    listenables: [Action],

    onAddDot(dots,content){
    	var t = this;
        var urls = 'http://121.43.148.191:8106/api/alarmlogs.json';
        if(location.host ===  "h5.mizlicai.com"){
            urls = '//h5.mizlicai.com/alarm/api/alarmlogs.json';
        }
        $.ajax({
            url: urls,
            type: 'post',
            dataType: 'json',
            data: {eventCode: dots,content:content,appId:'mzlc_h5'},
        })
        .fail(()=> console.error("打点失败"));
        t.google(dots,content);
    },

    google(dots,content){
        ((i, s, o, g, r, a, m)=> {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-77505833-1', 'auto');
        ga('send', 'pageview');

        ga('send', 'event', '', 'click', content);
        ga('send', 'event', '', 'click', location.href);

    },
});

module.exports = Store;