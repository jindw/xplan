var React = require('react');
// var lazyload = require('anima-lazyload');
var _map = require('lodash/map');

// var lazyload = require('jquery-lazyload');
require('jquery-image-lazyload');


var SwiperGrid = React.createClass({

	componentDidMount(){
		this.mySwiper();
	},

	componentDidUpdate(){
        // $("img").lazyload({
        //     threshold:1800,
        // });
        $.DrLazyload();
		this.mySwiper();
	},

	mySwiper(){
        if($('.swiper-slide-active').length>1)return false;
		new Swiper('.swipergrid',{
			direction : 'horizontal',
			slidesPerView : 'auto',
			slidesOffsetAfter:30,
			updateOnImagesReady : true,
            iOSEdgeSwipeDetection :true,
            iOSEdgeSwipeThreshold :30
		});
		// lazyload('img');
        // $("img").lazyload();
	},

    toTheme(id){//去主题
        localStorage.homeTop = $('body').scrollTop();
        location.hash = `/theme/${id}`;
    },

    toGoods(id){
        localStorage.homeTop = $('body').scrollTop();
    	location.hash = `/goods/${id}`;
    },

    render() {
        var t = this;
        var products = [];
        

        _map(t.props.products,function(itm,ind) {
            var _img =  <img data-lazy-src={itm.proImage} alt={itm.shortName} />;
            if(navigator.userAgent.toLowerCase().match(/MicroMessenger/i)=="micromessenger"){
                _img = <img src={itm.proImage} alt={itm.shortName} />;
            }
        	products.push(<div key={`sw_${itm.id}`} className="swiper-slide" onClick={t.toGoods.bind(null,itm.id)}>
                    {_img}
                    <div>
                        <span>{itm.shortName}</span>
                        <label>{itm.invest}</label>
                        <em>{`￥${itm.defaultPrice}`}</em>
                    </div>
                </div>);
        });
        if(t.props.products){
        	products.push(<div key={`theme_${t.props.themeId}`} className="swiper-slide" onClick={t.toTheme.bind(null,t.props.themeId)}>
					    <dl>
		                    <dt>查看全部</dt>
		                    <dd>See All</dd>
		                </dl>
	                </div>);
        }
        return (
            <section className = 'swiper-container swipergrid'>
            	<div className = 'swiper-wrapper'>
	        		{products}
	             </div>
            </section>
        );
    }
});

module.exports = SwiperGrid;