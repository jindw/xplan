var React = require('react');
var _times = require('lodash/times');
var _map = require('lodash/map');
var _includes = require('lodash/includes');
var AddDot = require('../../components/AddDot');
var loadover,first = true,move = true,si,ok=false,x1 = 0,x2 = 0,ff=true,auto;
var CanvasCarousel = React.createClass({

    componentDidMount() {
        this.first = true;
    },

    componentDidUpdate() {
        if(this.props.top.length&&this.first){
            new Swiper('.swiper-container', {
                autoplay: 5000,
                watchSlidesProgress : true,
                watchSlidesVisibility : true,
                direction : 'horizontal',
                autoHeight: true,
                pagination : '.swiper-pagination',
            });
            this.first = false;
        }
    },

    render() {
        var t = this;
        var slide = [];
        _map(this.props.top,(itm,ind)=>{
            slide.push(<div onClick={()=>{
                if(itm.url){
                    location.href = itm.url;
                }else if(itm.subjectId){
                    location.hash = `#/theme/${itm.subjectId}`;
                }
            }} key={`tp_${ind}`} className="swiper-slide">
            <img src={itm.imageUrl} style={{width:'100%'}}/>
            </div>);
        });

        var show_gonggao = _includes([392,386,385,384,383,382,381,380,379,378,377,372,371,368,313,260,259,258,247,244,100,97],this.props.idForGongGao)
        return <div className="swiper-container">
              <div className="swiper-wrapper">
                {slide}
              </div>
              <div className="swiper-pagination"/>
              <section style={{display:(show_gonggao?'none':'none')}} id='gonggao'>亲爱的庄主，为了给您带来更好的体验，杭州仓储在1月1日至1月7日进行升级，该宝贝预估在1月8号才能正常发货，请介意的亲亲慎拍哦</section>
            </div>
    }
});

module.exports = CanvasCarousel;