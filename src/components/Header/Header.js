var Signals = require('signals');
var Arrow_Right = require('../SvgIcon/Svg_Arrow_Right');
var Roll_Refresh = require('../SvgIcon/Svg_Roll_Refresh');
var MizShare = require('../../../bower_components/MizShare');
var Util = require('../../app/util');
var SendMsgs = require('../../components/SendMsgs');
var _assign = require('lodash/assign');
var React = require('react');


var Header = React.createClass({

    getInitialState() {
        return {
            title: '',
            backBtn: true,
            rightBtn: ''
        };
    },

    setHeader(cfg){
        var defaultSettings = {
            title: '',
            backBtn: true,
            rightBtn: ''
        };
        this.setState(_assign(defaultSettings, cfg));
    },

    back(){
        SendMsgs.XplanShowButtom(true);
        if(this.state.backBtn === 'app'){
            location.href = 'mzlicai://close';
            return;
        }
        if(window.history.length < 2){
            location.hash = '/#';
            return;
        }else if(location.hash.indexOf('#/login') === 0){
            location.href=location.origin+'/feature/xplan'
        }
        window.history.back();
        (history.length < 2)&&(location.hash = '/');
    },

    reloadPage: function(){
        window.location.reload();
    },

    componentDidMount : function(){
        var t = this;
        if(!window.__event__) window.__event__= {};
        __event__.setHeader = new Signals.Signal();
        __event__.setHeader.add(t.setHeader);
    },
    share:function(){
        MizShare.share();
    },

    toMyOrder(){
        if(localStorage.auth){
            location.href = `${location.origin}${location.pathname}#/order`;
        }else if(location.host !== "h5.mizlicai.com"){
            location.href = `//mizlicai.chinacloudsites.cn/#/login?backTo=${location.origin}${location.pathname}`;
        }else{
            location.href = `//h5.mizlicai.com/#/login?backTo=${location.origin}${location.pathname}`;
        }
    },

    render: function() {
        var t = this;
        var title = this.state.title;
        document.title=title;
        var back_dom = this.state.backBtn ? (<div className="flex-h jc-start ai-center arrow-back tTap" onClick={this.back}>
                        <Arrow_Right size={18} fill="#9c9cb6"/>
                        <div></div>
                    </div>) : <a className='intro' href='#/xplan'></a>;
        if(this.state.backBtn){
            SendMsgs.XplanShowButtom(false);
        }else{//首页
            SendMsgs.XplanShowButtom(true);
        }

        var right_btn;
        var noHeadTip_Btn;
        if(t.state.rightBtn){
            right_btn = <a onClick={t.toMyOrder} className='myorder' href='javascript:;'></a>
            noHeadTip_Btn = <i id='noHeadTip' className='myorders' onClick={t.toMyOrder}>我的<br/>订单</i>
        }else if(t.state.rightBtn !== false){
             right_btn = <a href="javascript:;" className="share" onClick={t.share}></a>
             noHeadTip_Btn = <i id='noHeadTip' className='share' onClick={t.share}></i>
        }

        if(window.__HEADER__){
            return (
                <div style={{display:(window.__HEADER__?'':'none')}} ref='header' className="flex-h jc-center ai-center header">
                    <div className="flex1">
                        {back_dom}
                    </div>
                    <div id="js_header_title" className="flex3 title">{title}</div>
                    <div className="flex1">
                        <div className="flex-h jc-start ai-center">
                            {right_btn}
                        </div>
                    </div>
                </div>
            );
        }else{
            return <section>{noHeadTip_Btn}</section>
        }
    }
});

module.exports = Header;