var PlusSvg = require('../SvgIcon/Svg_Plus');
var MinusSvg = require('../SvgIcon/Svg_Minus');
var Signals = require('signals');
var React = require('react');


var NumberBtn = React.createClass({

    getInitialState: function() {
        return {
            val: this.props.val||0
        };
    },

    componentDidMount : function(){
        var t = this;
        if(!window.__event__) window.__event__= {};
        __event__.numberBtnMinus = new Signals.Signal();
        __event__.numberBtnMinus.add(t.minus);
        this.plus();
    },
    plus: function(){
        var val = this.state.val === 0?this.props.val:this.state.val;
        val ++;
        var max = this.props.max;
        if(val > max){
            val = max;
        }
        this.setState({
            val: val
        });
        if(this.props.onSet){
            this.props.onSet(val);
        }
    },

    minus: function(fou){
        var val = this.state.val;
        val --;
        var min = this.props.min;
        if(val < min){
            val = min;
        }
        this.setState({
            val: val
        });
        if(this.props.onSet && !fou){
            this.props.onSet(val);
        }
    },

    componentDidUpdate(){
        var t = this;
        
        if(t.props.max < t.state.val){
            t.setState({
                val:t.props.max
            });
        }
    },

    render: function() {
        var t = this;
        var fillColorOff = '#ccc',fillColorOn = '#333'; 
        var val = t.state.val === 0?t.props.val:t.state.val;
        
       
        return (
            <div className="flex-h jc-start ai-center number">
                <div className="flex-h ai-center opt-btn left tTap" onClick={t.minus.bind(t, '')}>
                    <MinusSvg fill={val<=1?fillColorOff:fillColorOn}/>
                </div>
                <div className="show">{val}</div>
                <div className="flex-h ai-center opt-btn right tTap" onClick={t.plus}>
                    <PlusSvg fill={val === t.props.max?fillColorOff:fillColorOn}/>
                </div>
            </div>
        );
    }
});

module.exports = NumberBtn;