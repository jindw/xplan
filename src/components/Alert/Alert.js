var React = require('react');

var IosAlert = React.createClass({

    render(){
        var _t = this;
        var _title = _t.props.title || '系统提示';
        var _btntext = _t.props.btntext || '确定';
        var _content = _t.props.content;
        return (
            <div className={"ralert " + _t.props.className}>
                <div className="title">{_title}</div>
                <div className="content">{_content}</div>
                <div className="footer-btn">
                    <a className="click-link tTap" href="javascript:;" onClick={t.props.iknow}>{_btntext}</a>
                </div>
            </div>
        );
    }
});

module.exports = IosAlert;