/**************************************************\
                爆款版本选择
\**************************************************/
var Reflux = require('reflux');
var Store = require('./store');
var Action = require('./action');
var Modals = require('../../components/Modal');
var Util = require('../../app/util');
var React = require('react');


var MoreSelect = React.createClass({

    mixins: [Reflux.connect(Store)],

	componentDidUpdate (){
		var t = this;
		if(t.props.show === true){
        	t.openModal();
        }
	},
    openErrorModal() {
        this.setState({modalIsOpen: true});
    },

    closeErrorModal() {
        this.setState({modalIsOpen: false});
        this.state.onerror?history.go(-1):'';
    },

    getAlertContent(){
        var t = this;
        return (
            <div className="ralert myalert">
                <div className="content">{t.state.errmsg}</div>
                <div className="footer-btn">
                    <div className="click-link tTap" href="javascript:;" onClick={t.closeErrorModal}>确定</div>
                </div>
            </div>
        );
    },

	selecttype(type){
        var t = this;
        t.refs[type].getDOMNode().parentNode.getElementsByClassName('on')[0].className = '';
        t.refs[type].getDOMNode().className = 'on';
    },

    openModal(){
        this.refs.selectModal.getDOMNode().className = 'selectModal start';
        document.getElementsByClassName('selectModal')[0].addEventListener('touchmove',function(e){
        	e.preventDefault();
        })
    },

    closeModal(){
        this.refs.selectModal.getDOMNode().className = 'selectModal end';
    },

    buyNow(){
        var val = document.getElementsByClassName('on');
        var send;
        _.map(val,function(itm,ind){
            if(ind){
                send += ';'+val[ind].innerText;
            }else{
                send = val[ind].innerText;
            }
        });
        Action.buyNow(send,this.props.productid);
    },

    render: function() {
    	var t = this;
        var alist = t.props.data,list=[];
        _.map(alist, function(itm, ind){
            var lis = [];
            _.map(itm.attrValues, function(itms, inds){
                var mrel = itm.attrIndex+''+inds;

                var props = {
                    ref:mrel,
                    onClick:t.selecttype.bind(null,mrel),
                    className:inds === 0?'on':''
                };
                lis.push(
                    <li {...props}>{itms}</li>
                );
            });

            list.push(
                <div className='select'>
                    <label>{itm.attrName}：</label>
                    <ul>
                        {lis}
                    </ul>
                </div>
            );
        });

        return (
            <section ref = 'selectModal' className='selectModal'>
                <div>
                    <p className='title'>请选择商品规格
                    	<a href='javascript:;' onClick={t.closeModal}>&times;</a>
                    </p>
                    {list}
                </div>
                <a onClick={t.buyNow} href='javascript:;'>确定</a>
                <Modals isOpen={t.state.modalIsOpen} style={Util.customStyles}>
                    {t.getAlertContent()}
                </Modals>
            </section>
        );
    }
});

module.exports = MoreSelect;