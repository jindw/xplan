var Reflux = require('reflux');
var Action = require('./action');
var DB = require('../../app/db');

var Store = Reflux.createStore({

    listenables: [Action],

    onBuyNow:function(attr,productId){
        var t = this;
        DB.Activity.getDirectBuy({
            productId:productId,
            attrs:attr
        }).then(function(data){
            if(data.status === "I")
                location.hash='#/order/'+ data.orderId;
            else{
                t.data.modalIsOpen = true;
                t.data.errmsg = data.errorMsg;
                t.trigger(t.data);
            }
        },function(data){
            t.data.modalIsOpen = true;
            t.data.errmsg = data.errorMsg;
            t.trigger(t.data);
        });
    },

    getInitialState: function() {
        var t = this;
        t.data = {
            modalIsOpen: false,
            errmsg:''
        };
        return t.data;
    }
});

module.exports = Store;