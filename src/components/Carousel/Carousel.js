var React = require('react');

var Carousel = React.createClass({

    getDefaultProps: function(){
        return {
            imglist : []
        };
    },
    getInitialState:function() {
        return {
            playing : false
        }
    },

    componentDidMount : function(){
        var t = this;
        t.currentIndex = 0;
            var x;
            $('.carousel-container').on('touchstart',function(e) {
                x = e.targetTouches[0].clientX;
            }).on('touchmove',function(e) {
                if(Math.abs(x-e.targetTouches[0].clientX)>30){
                    if(t.scollHandle){
                        t.scollHandle.play();
                    }else{
                        t.next();
                    }
                }
            });
    },

    next: function() {
        var t = this;
        if(t.state.playing){
            return false;
        }
        t.setState({
            playing : true
        })
        var tag = t.props.tag;
        var el = $('#js_slider_'+tag);
        var handle = $('#js_slider_dot_'+tag);
        var count = t.props.imglist.length;
        if(el && el.length && handle && handle.length && count){
            t.scollToNext();
        }
    },

    setHandlerBar : function(){
        var t = this;
        var tag = t.props.tag;
        var handle = $('#js_slider_dot_'+tag);
        handle.find('.active').eq(0).removeClass('active');
        handle.children().eq(t.currentIndex-1).addClass('active');
    },

    currentIndex: 0,

    scollHandle: '',

    scollToNext: function(){
        var t = this;
        var tag = t.props.tag;
        var el = $('#js_slider_'+tag);
        var handle = $('#js_slider_dot_'+tag);
        var count = t.props.imglist.length;
        var winWidth = $(window).width();
        var autoplay = t.props.autoplay || true;
        t.currentIndex++;
        t.setHandlerBar();
        t.scollHandle = TweenLite.to(el, 0.7, {
            x: - t.currentIndex * winWidth,
            ease: Power2.easeInOut,
            delay: 0.1,
            onComplete: function(){
                if(t.currentIndex === count){
                    t.currentIndex = 0;
                    for (var i = 0; i < count; i++) {
                         el.children().eq(0).appendTo(el);
                    }
                    TweenLite.set(el, {
                        x : 0
                    });
                }
                t.scollHandle = '';
                t.setState({
                    playing : false
                })
            }
        });
    },

    componentWillUnmount: function(){
        // Carousel = '';
        // console.log('销毁');
        var t = this;
        t.currentIndex = 0;
        // t.scollHandle.pause();
    },

    render: function() {
        var t = this;
        var imgs = this.props.imglist;
        var winWidth = $(window).width();
        var imglist_dom = [];
        var imgdot_dom = [];
        if(imgs.length){
            var len = imgs.length;
            _.map(imgs, function(imgsrc, ind){
                imglist_dom[ind] = (
                    <div key={"carousel_img_"+ind} style={{width:winWidth+'px'}}>
                        <img src={imgsrc} height="300px"/>
                    </div>
                );
                imglist_dom[ind+len] = (
                    <div key={"carousel_img_"+(ind+len)} style={{width:winWidth+'px'}}>
                        <img src={imgsrc} height="300px"/>
                    </div>
                );
                imgdot_dom.push(
                    <div key={"carousel_dot_"+ind} className={ind==0?"carousel-dot active":"carousel-dot"}></div>
                );
            });
        }
        return (
            <div className="carousel-container">
                <div id={"js_slider_"+t.props.tag} className="carousel-inner" style={{width:winWidth*2*imgs.length+'px'}}>
                    {imglist_dom}
                </div>
                <div id={"js_slider_dot_"+t.props.tag} className="carousel-bar">
                    {imgdot_dom}
                </div>
                <i className='left' onClick={t.next}>&lt;</i>
                <i className='right' onClick={t.next}>&gt;</i>
            </div>
        );
    }
});

module.exports = Carousel;