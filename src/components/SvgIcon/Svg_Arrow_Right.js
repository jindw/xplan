var React = require('react');


var Svg = React.createClass({
    render: function() {
        var t = this;
        var size = t.props.size || 12;
        var fill = t.props.fill || '#333';
        return (
            <svg style={{width:size+"px",height:size+"px"}} viewBox="0 0 1024 1024" enableBackground="new 0 0 1024 1024">
                <path className="svgpath" fill={fill} d="M362.4585 486.9818 748.0193 873.143 701.0671 920.0932 315.3035 534.3467 315.3035 534.3467 270.0073 489.0524 713.77 45.3083 759.0662 90.6026Z"/>
            </svg>
        );
    }
});

module.exports = Svg;