var React = require('react');


var Svg = React.createClass({
    render: function() {
        var t = this;
        var size = t.props.size || 12;
        var fill = t.props.fill || '#333';
        return (
            <svg style={{width:size+"px",height:size+"px"}} viewBox="0 0 1024 1024" enableBackground="new 0 0 1024 1024">
                <path className="svgpath" fill={fill} d="M544 128 480 128 480 480 128 480 128 544 480 544 480 896 544 896 544 544 895.936 544 895.936 480 544 480Z"/>
            </svg>
        );
    }
});

module.exports = Svg;