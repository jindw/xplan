var React = require('react');


var Svg = React.createClass({
    render: function() {
        var t = this;
        var size = t.props.size || 12;
        var fill = t.props.fill || '#333';
        return (
            <svg style={{width:size+"px",height:size+"px"}} width="1024px" height="1024px" viewBox="0 0 1024 1024" enableBackground="new 0 0 1024 1024">
                <path className="svgpath" fill={fill} d="M128 480l767.936 0 0 64-767.936 0 0-64Z"/>
            </svg>
        );
    }
});

module.exports = Svg;