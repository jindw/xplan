var autosize = require('autosize');
var React = require('react');


var Textarea = React.createClass({
    handleChange: function(){
        var dom = this.refs.textarea,
            val = this.props.forbidFace ? this.forbidFace(dom) : dom.value;
        this.props.getValue(this.props.valueKey, val);
    },
    componentDidMount : function(){
        var t = this;
        autosize(t.refs.textarea);
    },
    forbidFace: function (target) {
        var em = '65306,65292,65288,65289,65281,65311'
        var i,
            ret = '',
            val = target.value,
            len = val.length;
        for (i = 0; i < len; i++) {
            if (val.charCodeAt(i) < 40908||em.indexOf(val.charCodeAt(i)) !== -1) {//不让输入表情
                ret += val[i];
            }
        }
        target.value = ret;
        return ret;
    },
    render: function (){
        var _className=this.props.className||'' +' form-textarea';
        return (
            <textarea onFocus={this.props.disabled?this.onBlur:null} readOnly={this.props.disabled||false} rows="1" ref="textarea" className={_className} placeholder={this.props.placeholder}  onChange={this.handleChange} value={this.props.defaultVal || ''}></textarea>
        )
    }
});

module.exports = Textarea;