var React = require('react');
var _times = require('lodash/times');

var m,pid,ok=false,loadover;
var CanvasImg = React.createClass({

    componentDidMount : function(){
        m = !(ok = false);
        if(this.props.dotLoad){
            this.dotLoading();
        }else{
            this.loading();
        }
        
    },

    componentWillMount(){
        loadover = false;
    },

    loading(){
        var c = this.refs.myimage;
        var ctx = c.getContext("2d");
        var img = new Image;
        img.src = 'http://miz-image.b0.upaiyun.com/xplan/car/car_1.jpg';

        c.width = document.body.scrollWidth;
        c.height = this.props.minHeight||document.body.scrollHeight;
        var loadsum = 0;
        var mimg = [];
        var imgs = [];
        for (var i = 0; i <5; i++) {
            mimg[i] = new Image();
            mimg[i].src = 'http://miz-image.b0.upaiyun.com/xplan/car/car_'+(i%5+1)+'.jpg';
            mimg[i].addEventListener('load',function(){
                loadsum++;
                if(loadsum === 5){
                    for (var ii = 0; ii < 5; ii++) {
                        imgs.push(mimg[ii]);
                    }
                    var is = 0;
                    if(m === false)return false;
                    m = setInterval(function(){
                        var left = (c.width-mimg[++is%5].width/2)/2;
                        if(m){
                            ctx.drawImage(mimg[is%5],left,c.height/3,img.width/2,img.height/2);
                        }
                    },100);
                }
            },false);
        }
        this.imgloadup();
    },

    dotLoading(){
        var t = this;
        var c = this.refs.myimage;
        var ctx=c.getContext("2d");
        var i = 1,big = true;
        c.width = document.body.scrollWidth;
        c.height = c.width/2;
        _times(3,function(ind) {
            var l = c.width/2-15*2+30*ind;
            setTimeout(loads(l),100*ind);
        });
        function loads(l){
            setInterval(function(){
                if(loadover){
                    return false;
                }
                ctx.clearRect(l-10,c.height/2-10,20,20)
                ctx.beginPath();
                ctx.fillStyle="#ccc";
                ctx.arc(l,c.height/2,i,0,2*Math.PI);
                ctx.fill();
                if(big){
                    i+=0.5;
                    i>=4&&(big = !big);
                }else{
                    i-=0.5;
                    i<=1&&(big = !big);
                }
            },150);
        }
    },

    componentDidUpdate(){
        this.imgloadup();
    },

    imgloadup(){
        var t = this,h = 0,sh = 0;
        var c = t.refs.myimage;
        var ctx=c.getContext("2d");
        var img = [];
        var imgs = [];
        var l = t.props.productpicture.length,i = 0;
        var e = 0;
        for (var i = 0; i < l; i++) {
            img[i] = new Image();
            img[i].src = t.props.productpicture[i];
            img[i].addEventListener('load',function(){
                e++;
                if(e === l){
                    for (var ii = 0; ii < l; ii++) {
                        imgs.push(img[ii]);
                        h += img[ii].height*img[0].width/img[ii].width;
                    }
                    imgin(l);
                    window.onresize = imgin(l);
                }
            });
        }

        function imgin(len){
            clearInterval(m);
            m = false;
            c.height = h;
            c.width = imgs[0].width;
            // c.width = document.body.scrollWidth;
            ok = true;
            sh = 0;
            for (var i = 0; i < len; i++) {
                var mh = imgs[i].height*c.width/imgs[i].width;
                ctx.drawImage(imgs[i],0,sh,c.width,mh);

                sh += mh;
                if(i+1 === len)
                    c.style.width = '100%';
            }
        }
    },

    render: function() {
        var t = this;

        return (
            <canvas key='canvasImg' style={{backgroundColor:'#fff'}} ref='myimage'/>
        );
    }
});

module.exports = CanvasImg;