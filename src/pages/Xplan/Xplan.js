/**************************************************\
                Xplan 攻略页面
\**************************************************/
var MizShare = require('../../../bower_components/MizShare');
var AddDot = require('../../components/AddDot');
var React = require('react');
var CanvasImg = require('../../components/CanvasImg');
var XpanPage = React.createClass({

    componentDidMount(){
        __event__.setHeader.dispatch({
            title: '米享计划'
        });
        AddDot.Dot('liaojiexplan_key');
        var shareObj = {
            sharedesc : '米享计划，理财购物两不误！',
            sharetext : '理财购物两不误',
            sharelineLink : `${location.origin}${location.pathname}#/xplan`,
            shareimgUrl : 'http://miz-image.b0.upaiyun.com/activities/mother/logo.png'
        };
        MizShare.config(shareObj);
        scroll(0,0);
        localStorage.homeTop = 0;
    },

    toHome(){
        location.hash='#/';
    },

    render() {
        var t = this;
        return (
            <section className='pinxuan'>
                <CanvasImg minHeight={document.body.scrollHeight} productpicture={['http://miz-image.b0.upaiyun.com/xplan/3.0/mshares.jpg']} />
                <em><a onClick={t.toHome} href='javascript:;'>进入米享计划</a></em>
            </section>
            )
    }
});

module.exports = XpanPage;