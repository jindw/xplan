/**************************************************\
                订单详情
\**************************************************/
var Reflux = require('reflux');
var Store = require('./store');
var Action = require('./action');
var Util = require('../../app/util');
var Modal = require('../../components/Modal');
var React = require('react');
var _includes = require('lodash/includes');
var _split = require('lodash/split');

var OrderDetlPage = React.createClass({

    mixins: [Reflux.connect(Store)],

    getInitialState() {
        Action.getOneOrder(this.props.params.orderId);
    },

    componentDidMount(){
        var t = this;
        __event__.setHeader.dispatch({
            title: '订单详情',
            rightBtn: false,
            backBtn:_includes(location.href,'type=1')?'app':true
        });
        Action.getNewLog(this.props.params.orderId);
        scroll(0,0);
    },
    sure(){
        Action.getCheckOrderLimit(this.state.order.xOrderNo);
    },
    closeModal(){
        this.setState({modalIsOpen:false});
    },
    getAlertContent(){
        var t = this;
        return (
            <div className="ralert myalert">
                <div className="content">{t.state.errmsg}</div>
                <div className="footer-btn">
                    <div className="click-link tTap" href="javascript:;" onClick={t.closeModal}>确定</div>
                </div>
            </div>
        );
    },

    orderStatusMap: {
        PAY_NOT: '未支付',
        P: '处理中',
        PAY_SUCCESS: '支付成功',
        PAY_FAIL: '支付失败',
        PAY_CANCEL: '已取消',
        PAY_OVER: '超额支付',
        PAY_REFUND: '已退款',
        PAY_DELIVER:'订单已发货',
        PAY_SIGN:'订单已签收'
    },
    orderStatusMsgMap: {
        PAY_NOT: '请于20分钟内完成支付操作',
        P: '请稍等',
        PAY_SUCCESS: '请耐心等待发货',
        PAY_FAIL: '请重试或联系米庄客服',
        PAY_CANCEL: '订单超时，请重试',
        PAY_OVER: '请耐心等待退款',
        PAY_REFUND: '请耐心等待短信通知',
        PAY_DELIVER:'如有疑问，请联系客服',
        PAY_SIGN:'如有疑问，请联系客服'
    },

    logistics(){
        location.hash = `/logistics/${this.props.params.orderId}`
    },

    render() {
        var t = this;
        var order = t.state.order;
        var fenqi = Util.getFenQi(order.investTime);
        var orderSt = t.state.lastTrace.status||order.status;

        // <div>{order.productAttrs&&(_split(order.productAttrs, ',', order.productAttrs.split(',').length).toString()||JSON.parse(order.productAttrs).attrValue||JSON.parse(order.productAttrs).attrValue1)}</div>
        return (
            <div className="order">
                <section className='status-top'>
                    <div>{t.orderStatusMap[orderSt]}</div>
                    <div>{t.orderStatusMsgMap[orderSt]}</div>
                    <i className={orderSt}></i>
                </section>
                <div className="piece status-consignee">
                    <div style={{display:(orderSt === 'PAY_DELIVER'||(t.state.lastTrace&&t.state.lastTrace.acceptStation)?'':'none')}} onClick={t.logistics} className="felx-v jc-center logistics" onClick={t.logistics}>
                        <div className="flex-h ai-center">
                            <div className="last-msg">{t.state.lastTrace&&t.state.lastTrace.acceptStation}</div>
                        </div>
                        <div className="ads-where">
                            {t.state.lastTrace&&t.state.lastTrace.acceptTime}
                        </div>
                    </div>

                    <div className="felx-v jc-center">
                        <div className="flex-h ai-center">
                            <div className="ads-name">收货人：{order.consignee}</div>
                            <div className="ads-phone">{order.mobile}</div>
                        </div>
                        <div className="ads-where">
                            收货地址：{order.address}
                        </div>
                    </div>
                </div>
                <div className="piece flex-v margTop10 myproduct">
                    <div className="product-title">{order.productName}</div>
                    <div className="flex-h jc-start">
                        <div>
                            <img src={order.productImage} width="84px" height="84px"/>
                        </div>
                        <div className="flex-v jc-center product-msg margLeft15">
                            <div className="flex-h jc-start ai-center margBottom10">
                                <div>规格属性：</div>
                                <div>{order.productAttrs}</div>
                                
                            </div>
                            <div className="flex-h jc-start ai-center margBottom10">
                                <div>购买数量：</div>
                                <div>x{order.productNum}</div>
                            </div>
                            <div className="flex-h jc-start ai-center margBottom10">
                                <div>实际存入：</div>
                                <div>{order.amount}元{order.investCycle}</div>
                            </div>
                            <div className="flex-h jc-start ai-center margBottom10">
                                <div>另享收益：</div>
                                <div>{order.income}元</div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div style={{display:(order.remark!==''?'flex':'none')}} className="remark">
                    <div>买家留言：</div>
                    <div>
                        {order.remark}
                    </div>
                </div>
                <span>温馨提示：资产详情请至【我的资产】-【定期产品】中查看</span>
                <div className={"sure-btn " + (order.status === 'PAY_NOT' ? "" : "hide")}>
                    <a className="tTap btn btn-default btn-lg" href="javascript:;" onClick={t.sure}>继续支付</a>
                </div>
                <dl>
                    <dt>客服热线:<a href='tel:4006998883'>400-699-8883</a></dt>
                    <dd>服务时间:每日09:00-21:00</dd>
                </dl>
                <Modal isOpen={t.state.modalIsOpen}
                        onRequestClose={t.closeModal}
                        style={Util.customStyles}>
                    {t.getAlertContent()}
                </Modal>
            </div>
        );
    }
});

module.exports = OrderDetlPage;