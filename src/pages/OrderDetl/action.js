var Reflux = require('reflux');

module.exports = Reflux.createActions([
    'getOneOrder', // 获取订单详情
    'getCheckOrderLimit',//获取库存
    'getNewLog'//最新物流
]);