var Reflux = require('reflux');
var Action = require('./action');
var DB = require('../../app/db');
var Util = require('../../app/util');

var Store = Reflux.createStore({

    listenables: [Action],

    data: {},

    onGetNewLog(xOrderNo){
        var t = this;
        DB.Order.newLoGistic({
            xOrderNo
        }).then(data=>{
            t.data.lastTrace = data.lastTrace;
            t.trigger(t.data);
        });
    },

    onGetOneOrder(orderId){
        var t = this;
        DB.Order.getOneById({
            orderId
        }).then(data=>{
            t.data.order = data || [];
            t.trigger(t.data);
        },data=>{
            t.data.errmsg = data.errorMsg;
            t.data.modalIsOpen = true;
            t.trigger(t.data);
        });
    },
    onGetCheckOrderLimit(orderNo){
        var t = this;
        DB.Order.rePay({
            orderNo
        }).then(data=> {
            location.href = `${data}&returnUrl=${location.origin}${location.pathname}`;
        },data=>{
            t.data.errmsg = data.errorMsg;
            t.data.modalIsOpen = true;
            t.trigger(t.data);
        });
    },

    getInitialState() {
        var t = this;
        this.data = {
            order : {
                productAttrs:'',
            },
            modalIsOpen:false,
            errmsg:'',
            lastTrace:[],
        };
        return this.data;
    }
});

module.exports = Store;