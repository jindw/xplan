/**************************************************\
                    确认订单
\**************************************************/
var Reflux = require('reflux');
var Store = require('./store');
var Action = require('./action');
var DB = require('../../app/db');
var Util = require('../../app/util');

var Textarea = require('../../components/Textarea');
var Modal = require('../../components/Modal');
var NumberBtn = require('../../components/NumberBtn');
var AddDot = require('../../components/AddDot');
var React = require('react');
var _split = require('lodash/split');
var autosize = require('autosize');

var Address = React.createClass({

    mixins: [Reflux.connect(Store)],

    getInitialState() {
        this.address();
    },

    address(){
        var addressId = sessionStorage.addressId;
        if(addressId){
            Action.info(addressId);
        }else{
            Action.getAddress();
        }
    },

    addAddress(){
        location.href = `${location.origin}/feature/miz-address/#/operate?from=${this.from}`;
    },

    changeAddress(){
        sessionStorage.back = location.href;
        location.href = `${location.origin}/feature/miz-address/#/?from=${this.from}`;
    },

    componentWillUnmount() {
        sessionStorage.addressId ='';
        clearInterval(this.getAddress);
    },

    componentDidMount() {
        if(/iPhone/i.test(navigator.userAgent)&&navigator.platform !== 'Win32'){
            this.getAddress = setInterval(()=>this.address(),2500);
        }
        this.from = window.__HEADER__?'':'xplan';
    },

    render() {
        var t = this;
        var address_dom = '';
        if(this.state.address){
            address_dom = (
                <div className="felx-v jc-center">
                    <div className="flex-h ai-center">
                        <div className="ads-name">收货人：{this.state.name}</div>
                        <div className="ads-phone">{this.state.phone}</div>
                    </div>
                    <div className="ads-where">
                        收货地址：{`${this.state.province||''} ${this.state.city||''} 
                        ${this.state.county||''} ${this.state.address}`}
                    </div>
                   
                    <div className="ads-change">
                        <a onClick={this.changeAddress} href="javascript:;" 
                        className="a-link">更换收货人</a>
                    </div>
                </div>
            );
        }else{
            var load;
            if(!t.state.addressDone){
                load=(<div className="ads-none">
                        loading...
                    </div>);
            }else{
                load = (<div className="ads-none">
                            提示：您还没有设置收货地址，<a href="javascript:;" onClick={this.addAddress} className="a-link">点击添加</a>
                        </div>);
            }

            address_dom = (
                <div className="felx-h ai-center">
                    {load}
                </div>
            );
        }

        return (
            <div className="piece margTop10 status-consignee">
                {address_dom}
            </div>
        );
    }
});


var OrderPage = React.createClass({

    mixins: [Reflux.connect(Store)],

    getInitialState(){
        Action.checkOrder(this.getHash('num'),this.getHash('productDetailId'));
    },

    handleReturnValue(key, val){
        var t = this;
        var temp = {};
        temp[key] = val;
        t.setState(temp);
    },

    sure(){
        AddDot.Dot('xplan_orderbuy_key');
        var t = this;
        if(!this.state.address){
            this.setState({
                modalIsOpen:true,
                tip:{
                    msg:'您的地址信息不全，请点击更换收货人完善地址',
                },
            })
            return;
        }
        var query = {
            addressId:t.state.id,
            // remark: t.state.remark,
            remark:t.refs.req.value,
            buyNum:t.getHash('num'),
            productDetailId:t.getHash('productDetailId')
        };

        Action.getNOrder(query);
    },

    openModal() {
        this.setState({modalIsOpen: true});
    },

    closeModal() {
        if(this.state.onerror){
            history.go(-1);
        }
        this.setState({modalIsOpen: false});
    },

    getAlertContent(){
        var t = this;
        var tip = t.state.tip;
        return (
            <div className={"ralert myalert"}>
                <div className="title">米庄理财提示</div>
                <div className="content">{tip.msg}</div>
                <div className="footer-btn">
                    <a className="click-link tTap" href="javascript:;" onClick={t.closeModal}>确定</a>
                </div>
            </div>
        );
    },

    componentDidMount(){
        __event__.setHeader.dispatch({
            title: '确认订单',
            rightBtn: false
        });
        scroll(0,0);
        autosize(this.refs.req)
    },

    getHash(item){
        var start = location.hash.indexOf(item) + item.length + 1;
        var end = location.hash.substring(start).indexOf('&');
        return location.hash.substring(start).substring(0,end);
    },

    onError(itm){
        this.refs[itm].src = '//miz-image.b0.upaiyun.com/xplan/car/car.gif';
    },

    render() {
        var t = this;
        var prod = t.state.order;
        var product=(<div className="piece flex-v margTop10 myproduct">
                <div className="product-title">{t.state.productName}</div>
                <div className="flex-h jc-start">
                    <div>
                        <img onError={t.onError.bind(null,'s_img')} ref='s_img' src={t.state.proImage}  width="84px" height="84px"/>
                    </div>
                    <div className="flex-v jc-center product-msg margLeft15">
                        <div className="flex-h jc-start ai-center margBottom10">
                            <div>规格属性：</div>
                            <div>{t.state.attrs}</div>
                        </div>
                        <div className="flex-h jc-start ai-center margBottom10">
                            <div>购买数量：</div>
                            <div>x{t.getHash('num')}</div>
                        </div>
                        <div className="flex-h jc-start ai-center margBottom10">
                            <div>实际存入：</div>
                            <div>{t.state.investAmount}元</div>
                        </div>
                        <div className="flex-h jc-start ai-center margBottom10">
                            <div>另享收益：</div>
                            <div>{t.state.income}元</div>
                        </div>
                        
                    </div>
                </div>
            </div>);

        // <Textarea disabled={prod.status === 'U'?true:false} placeholder="选填，您的特殊要求，如配送方式等" getValue={t.handleReturnValue} valueKey="remark" defaultVal={prod.status === 'U'?t.state.order.remark:t.state.remark} forbidFace={true}></Textarea>

        return (
            <section className='orderNew'>
                <div className="order morder">
                    <Address {...this.props}/>
                    {product}
                    <div style={{'paddingTop':'10px'}} className="piece flex-h jc-start">
                        <div className="textarea-label">配送时间</div>
                        <div className="fahuo">
                            {this.state.deliverDesc||'3-15个工作日发货'}
                        </div>
                    </div>
                    <div style={{display:((prod.status === 'U'&&!t.state.order.remark)?'none':'-webkit-flex'),'paddingTop':'10px'}} className="piece flex-h jc-start">
                        <div className="textarea-label">买家留言：</div>
                        <div className="textarea-box">
                            
                            <textarea ref='req' placeholder="选填，您的特殊要求，如配送方式等"/>
                        </div>
                    </div>
                </div>
               
                <div className="mybtn">
                    <label>合计：<em>&yen;{t.state.investAmount}元</em></label>
                    <a href="javascript:;" onClick={t.sure}>提交订单</a>
                </div>
                <Modal isOpen={t.state.modalIsOpen}
                        onRequestClose={t.closeModal}
                        style={Util.customStyles}>
                    {t.getAlertContent()}
                </Modal>
            </section>
        );
    }
});

module.exports = OrderPage;