var Reflux = require('reflux');
var Action = require('./action');
var DB = require('../../app/db');
var Util = require('../../app/util');
var _assign = require('lodash/assign');

var Store = Reflux.createStore({

    listenables: [Action],

    data: {},

    info(addressId){
        var t = this;
        DB.Address.info({addressId}).then(data=> {
            if(data.address.isActive){
                _assign(t.data,data.address);
                t.trigger(t.data);
            }else{
                t.getAddress();
            }
        }, data=>{
            t.data.modalIsOpen = true;
            t.data.tip.msg = data.errorMsg;
            t.trigger(t.data);
        });
    },

    onCheckOrder(buyNum,productDetailId) {
        var t = this;
        DB.Goods.checkOrder({
            buyNum,
            productDetailId,
        }).then(data=> {
            var re = data;
            if(re){
                t.data.investAmount = re.investAmount;
                t.data.productName = re.productName;
                t.data.income = re.income;
                t.data.attrs = re.attrs;
                t.data.proImage = re.proImage;
                t.data.deliverDesc = re.deliverDesc;
            }else{
                t.data.modalIsOpen = true;
                t.data.tip.msg = data.errorMsg;
                t.data.onerror = true;
            }
            t.trigger(t.data);
        });
    },

    onGetNOrder(query) {
        var t = this;
        t.data.modalIsOpen = true;
        t.data.tip.msg = '处理中';
        t.trigger(t.data);
        DB.Order.getNorder(query).then(data=> {
            t.data.modalIsOpen = false;
            location.href = `${data.xOrderNo}&returnUrl=${location.origin}${location.pathname}`;
        }, data=> {
            if (data && (data.status === 'FAILED'||data.status === 'ERROR')) {
                t.data.modalIsOpen = true;
                t.data.tip.msg = data.errorMsg;
                t.trigger(t.data);
            }
        });
    },

    getAddress() {
        var t = this;
        DB.Address.default().then(data=> {
            if(data.address){
                _assign(t.data,data);
            }else{
                this.data.address = null;
            }
            t.data.addressDone = true;
            t.trigger(t.data);
        }, data=>{
            t.data.modalIsOpen = true;
            t.data.tip.msg = data.errorMsg;
            t.trigger(t.data);
        });
    },

    updateComponent(options) {
        options = options || {};
        this.trigger(this.data);
    },

    getInitialState() {
        var t = this;
        this.data = {
            order: {},
            addressDone: false,
            remark: '',
            hasSubmit: false,
            modalIsOpen: false,
            tip: {
                msg: '',
                type: ''
            },
            investAmount:0,
            income:0,
            productName:'-',
            attrs:'-',
            proImage:null,
            onerror:false,
            couldReDefaultAddress:true,
        };
        return this.data;
    }
});

module.exports = Store;