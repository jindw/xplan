var Reflux = require('reflux');
var Action = require('./action');
var DB = require('../../app/db');

var Store = Reflux.createStore({

    listenables: [Action],

    onGetOneLog(id){
        var t = this;
        DB.Order.loGistic({
            xOrderNo:id
        }).then(data=> {
            t.data.logistics = data.logistics;
            t.data.defaultTrace = data.defaultTrace;
            if(!data.logistics){
                t.data.modalIsOpen = true;
                t.data.errmsg = '未查询到物流信息';
                t.data.onerror = true;
            }
            t.trigger(t.data);
        },data=> {
            t.data.modalIsOpen = true;
            t.data.errmsg = '未查询到物流信息';
            t.data.onerror = true;
            t.trigger(t.data);
        })
    },

    getInitialState() {
        var t = this;
        this.data = {
            logistics : [],
            modalIsOpen:false,
            errmsg:'',
            onerror:false
        };
        return this.data;
    }
});

module.exports = Store;