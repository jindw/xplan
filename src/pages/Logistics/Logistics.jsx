/**************************************************\
                物流详情
\**************************************************/
var React = require('react');
var Reflux = require('reflux');
var Action = require('./action');
var Store = require('./store');
var _map = require('lodash/map');
var Modal = require('../../components/Modal');
var Util = require('../../app/util');

var Logistics = React.createClass({

    mixins: [Reflux.connect(Store)],

    getInitialState(){
        Action.getOneLog(this.props.params.id);
    },

	componentDidMount(){
		__event__.setHeader.dispatch({
            title: '查看物流',
            rightBtn: false
        });
        scroll(0,0);
	},

    openModal() {
        this.setState({modalIsOpen: true});
    },

    closeModal() {
        this.setState({modalIsOpen: false});
        this.state.onerror?history.go(-1):'';
    },

    getAlertContent(){
        var t = this;
        return (
            <div className="ralert myalert">
                <div className="content">{t.state.errmsg}</div>
                <div className="footer-btn">
                    <div className="click-link tTap" href="javascript:;" onClick={t.closeModal}>确定</div>
                </div>
            </div>
        );
    },

    render() {
        var t = this;
        var logs = t.state.logistics;
        var defaultTrace = t.state.defaultTrace;
        var mlog = [];
        if(logs&&logs.shipInfo&&JSON.parse(logs.shipInfo).length){
            _map(JSON.parse(logs.shipInfo),(itm,ind)=>mlog.push(<li key={`mlog_${ind}`}>{itm.acceptStation}<div>{itm.acceptTime}</div></li>))
        }
        if(defaultTrace){
            var time = defaultTrace.acceptTime;
            mlog.push(<li key={'mlog'}>{defaultTrace.acceptStation}<div style={{display:(time?'':'none')}}>{time}</div></li>)
        }

        return (
            <section className = 'logistics'>
                <div className='topmsg'>
                    <img src={logs&&logs.productImage} />
                    <ul>
                        <li>物流状态:<label>{logs&&logs.statusVal}</label></li>
                        <li>承运来源:<span>{logs&&logs.shipExpName}</span></li>
                        <li>订单编号:<span>{logs&&logs.shipNo}</span></li>
                        <li style={{display:'none'}}>官方电话:<span>110</span></li>
                    </ul>
                </div>
                <div className='logistics-list'>
                    <ul>
                        {mlog}
                    </ul>
                </div>
                <Modal isOpen={t.state.modalIsOpen}
                    onRequestClose={t.closeModal}
                    style={Util.customStyles}>
                    {t.getAlertContent()}
                </Modal>
            </section>
        );
    }
});

module.exports = Logistics;