/**************************************************\
                新商品详情页
\**************************************************/
var NumberBtn = require('../../components/NumberBtn');
var CanvasCarousel = require('../../components/CanvasCarousel');
var Reflux = require('reflux');
var Store = require('./store');
var Action = require('./action');
var _map = require('lodash/map');
var _split = require('lodash/split');
var _ceil = require('lodash/ceil');
var _multiply = require('lodash/multiply');

var Util = require('../../app/util');
var Modal = require('../../components/Modal');
var AddDot = require('../../components/AddDot');
var React = require('react');
var Link = require('react-router').IndexLink;

var Goods = React.createClass({

    mixins: [Reflux.connect(Store)],

    getInitialState(){
        Action.getGoods(this.props.params.id);
    },

    componentDidMount(){
        __event__.setHeader.dispatch({
            title: '商品详情'
        });
        scroll(0,0);
        // location.href ='http://mizlicai.chinacloudsites.cn/feature/xplan/one/115/';
    },

    toSelect(it){
        var t = this;
        if(!t.refs[it].className){
            t.refs[it].parentNode.getElementsByClassName('on')[0].className = '';
            t.refs[it].className = 'on';
            t.productAttr();
        }
    },

    productAttr(){
        var ids = [],cycle;
        var doms = document.querySelectorAll('.select .on');
        _map(doms,(itm,ind)=> ids.push(itm.getAttribute('data-id')));
        Action.getProductAttr(ids.toString(),this.state.num);
    },

    toBuy(){
        // location.href='/?payNumber=""&source=""#/products'
        // return;
        if(!this.state.buyLimit)return;
        var doms = document.querySelectorAll('.select .on');
        var attrlook = [],attrsend=[],cyclelook,cyclesend;
        _map(doms,(itm,ind)=> {
            attrsend.push(itm.getAttribute('data-id'));
            if(ind+1<doms.length){
                attrlook.push(itm.innerText);
            }else{
                cyclelook = itm.innerText;
            }
        });
        AddDot.Dot('xplan_buy_key');
        Action.checkOrder(this.state.num,this.state.productDetailId);
    },

    moreSelect(itms,inds){
        var t = this;
        var dd = [];

        var doms = document.querySelectorAll('.select .on');
        var ids = [];
        _map(doms,(itm,ind)=> ids.push(itm.getAttribute('data-id')));
        
        _map(itms,(itm,ind)=>dd.push(<dd key={`ms_${ind}`}
            data-id={itm.id}
            className={ids[inds]?(+ids[inds] === itm.id?'on':''):(ind?'':'on')}
            ref={`${itm.id}_${itm.attrValue}`}
            onClick={t.toSelect.bind(null,`${itm.id}_${itm.attrValue}`)}>{itm.attrValue}</dd>));

        return <div key={`${itms[0].attrName}_${Math.random()}`}>
                    <label>{itms[0].attrName}</label>
                    <dl>
                       {dd}
                    </dl>
                </div>
    },

    cycle(itms){
        var t = this;
        var dd = [];
        _map(itms,(itm,ind)=>dd.push(<dd key={`cycle_${ind}`}
                data-id={itm.cycle}
                className={ind?'':'on'}
                ref={itm.serial}
                onClick={t.toSelect.bind(null,itm.serial)}>{itm.cycleValue}</dd>));

         return <div>
                    <label>期限：</label>
                    <dl>
                       {dd}
                    </dl>
                </div>
    },

    onSelectNumber(val){
        this.setState({num:val});
    },

    // openModal() {
    //     this.setState({modalIsOpen: true});
    //     // console.log(9)
    //     // console.log(this.refs.errmsg)
    // },

    closeModal() {
        this.setState({modalIsOpen: false});
        this.state.onerror?history.go(-1):'';
    },

    getAlertContent(){
        var t = this;
        return (
            <div className="ralert myalert">
                <div className="content" ref='errmsg' dangerouslySetInnerHTML = {{__html:t.state.errmsg}}></div>
                <div className="footer-btn">
                    <div className="click-link tTap" href="javascript:;" onClick={t.closeModal}>确定</div>
                </div>
            </div>
        );
    },

    proveIncome(){
        AddDot.Dot('xplan_upincome_key');
    },

    render() {
        var t = this;
        var top = [];

        var selection = [],products = [],title=[];
        var txt = '存钱免费拿';
        if(t.state.product){
            var p = t.state.product;

            var start = new Date(p.startDate);
            
            if(new Date()<start){
                if(start.getDate() === new Date().getDate()){
                    txt = `今日${start.getHours()}:${start.getMinutes()>10?start.getMinutes():'0'+start.getMinutes()}开抢`;
                }else if(start.getDate() - (new Date()).getDate()>1){
                    txt = `${start.getMonth()+1}月${start.getDate()}日 ${start.getHours()}:${start.getMinutes()>10?start.getMinutes():'0'+start.getMinutes()}开抢`;
                }else{
                    txt = `明日${start.getHours()}:${start.getMinutes()>10?start.getMinutes():'0'+start.getMinutes()}开抢`;
                }
            }
            
            _map(p.caroImages,(itm,ind)=>top.push({imageUrl:itm}));
            products = _split(p.descImage,';',_split(p.descImage,';').length-1);
            products.push('//miz-image.b0.upaiyun.com/xplan/3.0/faq.png');
            products.unshift('//miz-image.b0.upaiyun.com/xplan/3.0/play.jpg');

            var productAttrs = [];
            _map(t.state.product.productAttrs,(itm,ind)=> {
                if(!productAttrs[itm.attrRow - 1]){
                    productAttrs[itm.attrRow - 1] = [];
                }
                productAttrs[itm.attrRow - 1].push(itm);
            })
            _map(productAttrs,(itm,ind)=>selection.push(t.moreSelect(itm,ind)));

            selection.push(<div key='num'>
                        <label>数量</label>
                        <NumberBtn disabled = {t.state.buyLimit===0}
                        min={t.state.buyLimit>0?1:0}
                        max={t.state.buyLimit}
                        onSet={t.onSelectNumber}
                        val={t.state.buyLimit>0?1:0} />
                    </div>);
            title.push(<div key='title' className='title'>
                        <label>{p.productName}</label>
                        <span>市场价：
                            <label>{t.state.marketPrice}元</label>
                        </span>
                        <em><i style={{display:'none'}}>{t.state.firstPrice}</i><span style={{display:'none'}}>元起存免费拿</span></em>
                        <p>{p.description}</p>
                    </div>);
        }

        var cont_h = ($('body').height() - $('.header').height())||'auto';
        var prosuctImgs = [];
        _map(products,function(itm,ind) {
            prosuctImgs.push(<img key={`products_${ind}`} style={{width:'100%',marginBottom:'-2px'}} src={itm}/>);
        });

        return (
            <section ref='mygoods' className = 'mygoods'>
                <div className='contain'>
                    <CanvasCarousel loading={true}
                    idForGongGao={t.state.product&&t.state.product.id}
                    big='false'
                    minHeight={document.body.scrollHeight/3}
                    name='goods'
                    top={top} 
                    topMax={top.length||0}
                    sdotcolor='#ccc'
                    dotstyle='stroke'/>

                    <div className='forimg'>
                        {title}
                        <div key='select' className='select'>
                            {selection}
                        </div>
                        <dl>
                            <dd key='min'>存入本金
                                <label>
                                    <span ref='min'>{_ceil((t.state.num||1)*t.state.price,2)}</span>元
                                </label>
                            </dd>
                            <dd key='mout'>额外收益
                                <label>
                                    <span ref='mout'>{_ceil(_multiply(t.state.profit, (t.state.num||1)),2)}</span>元
                                </label>
                            </dd>
                            <a style={{display:''}} href='#/xplan' onClick={t.proveIncome}>
                                <span>米享计划如何做到收益增值，预期年化更高？</span>
                            </a>
                        </dl>
                        <div key='detail' className='detail' style={{display:(t.state.product&&t.state.product.caroImage?'none':'none')}}>
                            <p>图文详情</p>
                        </div>
                        {prosuctImgs}
                    </div>
                </div>
                <a onClick={t.toBuy}
                href='javascript:;'
                className='toBuyNow'
                style={{backgroundColor:t.state.buyLimit===0?'#ccc':'#FF5600'}}>{txt}</a>
                <Modal isOpen={t.state.modalIsOpen}
                    onRequestClose={t.closeModal}
                    style={Util.customStyles}>
                    {t.getAlertContent()}
                </Modal>
            </section>
        );
    }
});

module.exports = Goods;