var Reflux = require('reflux');
var Action = require('./action');
var DB = require('../../app/db');
var Util = require('../../app/util');
var _map = require('lodash/map');
var _floor = require('lodash/floor');
var _min = require('lodash/min');

var MizShare = require('../../../bower_components/MizShare');

var Store = Reflux.createStore({

    listenables: [Action],

    onCheckOrder(num,productDetailId) {
        var t = this;
        DB.Goods.checkOrder({
            userKey: Util.getUserKey(),
            buyNum: num,
            productDetailId,
        }).then(data=> {
            if(!data){
                t.data.modalIsOpen = true;
                t.data.errmsg = data.errorMsg;
                t.trigger(t.data);
            }else{
                location.hash = `ordernew?num=${num}&productDetailId=${t.data.productDetailId}`;
            }
        }, data=>{
            t.data.modalIsOpen = true;
            var errorMsg = data.errorMsg;
            if(data.status === 'PRODUCT_CHECK_ERROR'&&navigator.userAgent.toLowerCase().match(/MicroMessenger/i)=="micromessenger"){
                errorMsg = '如需购买，请拨打客服热线：<br/>400-699-8883。';
            }
            t.data.errmsg = errorMsg;
            t.trigger(t.data);
        });
    },

    onGetGoods(id){
        var t = this;
        DB.Goods.getGoods({
            id : id
        }).then(data=> {
        	t.data.product = data.product;
            t.data.shareMsg = data.product.shareMsg;
            t.trigger(t.data);
            if(t.data.first){
                var i = 0,itms=[];
                _map(data.product.productAttrs,(itm,ind)=>i!==itm.attrRow&&itms.push(itm.id)&&i++);
                t.onGetProductAttr(itms.toString(),1);
            }
            var shareObj = {
                sharedesc : '米享计划，理财购物两不误！',
                sharetext : data.product.shareMsg,
                sharelineLink : `${location.origin}${location.pathname}#/goods/${id}`,
                shareimgUrl : 'http://miz-image.b0.upaiyun.com/activities/mother/logo.png'
            };
            MizShare.config(shareObj);
        },data=>{
            t.data.errmsg = data.errorMsg;
            t.data.modalIsOpen = true;
            t.data.onerror = true;
            t.data.first = false;
            t.trigger(t.data);
        });
    },
    onGetProductAttr(id,num){
        var t = this;
        DB.Goods.getProductAttr({
            attrIds : id
        }).then(data=>{
            t.data.first = false;
            t.data.price = data.amount||0;
            t.data.profit = data.income||0;
            var limit = _floor(_min([data.buyLimit, data.stock]));
            t.data.buyLimit = limit;
            t.data.marketPrice = data.marketPrice;
            t.data.productDetailId = data.productDetailId;
            !t.data.firstPrice&&(t.data.firstPrice = data.amount||0);
            t.data.num = limit?((num<limit&&num)?num:(num?limit:1)):0;
            t.data.modalIsOpen =  false;
            t.trigger(t.data);
        },data=>{
            t.data.modalIsOpen =  true;
            t.data.errmsg = data.errorMsg;
            t.data.first = false;
            t.trigger(t.data);
        });
    },

    getInitialState() {
        var t = this;
        this.data = {
            num:0,
            price:0,
            profit:0,
            product:null,
            first:true,
            buyLimit:0,
            marketPrice:0,
            modalIsOpen: false,
            errmsg:'',
            onerror:false,
            shareMsg:'-',
            firstPrice:0
        };
        return this.data;
    }
});

module.exports = Store;