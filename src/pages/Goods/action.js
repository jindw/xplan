var Reflux = require('reflux');

module.exports = Reflux.createActions([
    'getGoods',//获取数据
    'getProductAttr',//价格
    'checkOrder'//提交
]);