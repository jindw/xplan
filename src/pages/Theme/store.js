var Reflux = require('reflux');
var Action = require('./action');
var DB = require('../../app/db');
var MizShare = require('../../../bower_components/MizShare');

var Store = Reflux.createStore({

    listenables: [Action],

    onGetThemeDetail(id){
        var t = this;
        DB.Product.getTheme({
            id
        }).then(data=> {
            t.data.subject = data.subject;
            t.data.subjectModules = data.subjectModules;
            t.trigger(t.data);
            var shareObj = {
                sharedesc : '米享计划，理财购物两不误！',
                sharetext : data.subject.shareMsg,
                sharelineLink : `${location.origin}${location.pathname}#/theme/${id}`,
                shareimgUrl : 'http://miz-image.b0.upaiyun.com/activities/mother/logo.png'
            }; 
            MizShare.config(shareObj);
            __event__.setHeader.dispatch({
                title: data.subject.subjectName||'主题推荐'
            });
        });
    },

    getInitialState() {
        var t = this;
        this.data = {
            subject : null,
            subjectModules:null,
            maxl:Math.floor((document.body.offsetWidth-54)/13)*3-6
        };
        return this.data;
    }
});

module.exports = Store;