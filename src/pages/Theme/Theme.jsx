/**************************************************\
                主题页面
\**************************************************/

var Reflux = require('reflux');
var Store = require('./store');
var Action = require('./action');
var _map = require('lodash/map');
var React = require('react');
var Textarea = require('../../components/Textarea');
var AddDot = require('../../components/AddDot');

var Theme = React.createClass({

    mixins: [Reflux.connect(Store)],

	componentDidMount(){
        // if(navigator.appVersion.indexOf('iPhone')!==-1){
        // console.log(document.title)
        if(window.__IOSAPP__&&document.title){
            __event__.setHeader.dispatch({
                title: '主题推荐'
            });
        }

        Action.getThemeDetail(this.props.params.id);
        var t = this;

        window.onresize = ()=>{
            t.setState({
                maxl:Math.floor((document.body.offsetWidth-54)/13)*3-6
            });
        }
        AddDot.Dot('xplan_theme_key',this.props.params.id);
        scroll(0,0);
	},

    isGoods(pro,title){
        var t = this;
        var msg = pro.shortDescription;
        return <div key={`title_${pro.id}`} className='recommend'>
                <em>{title}</em>
                <div>
                    <a href = {`${location.origin}${location.pathname}#/goods/${pro.id}`}></a>
                    <div className='con'>
                        <img src={pro.proImage}/>
                        <div>
                            <dl>{pro.productName}</dl>
                            <dl>
                                <dt>
                                    <span>{pro.saleDesc}</span>
                                </dt>
                                <a href='javascript:;'>去购买</a>
                            </dl>
                        </div>
                    </div>
                    <dd>
                      {msg.substring(0,t.state.maxl)}
                      {msg.length>t.state.maxl?'......':''}
                      <span style={{display:(msg.length>t.state.maxl?'':'none')}}>查看详情</span>
                    </dd>
                </div>
            </div>
    },

    date(time){
        var tt = new Date(time);
        return tt.getFullYear()+"-"+(tt.getMonth()>=9?tt.getMonth()+1:'0'+(tt.getMonth()+1))+"-"+(tt.getDate()>9?tt.getDate():'0'+tt.getDate());
    },

    onError(itm,small){
        var t = this;
        t.refs[itm].src = 'http://miz-image.b0.upaiyun.com/xplan/car/car.gif';
        if(small === true){
            t.refs[itm].style.width = '30%';
            t.refs[itm].style.marginLeft = '35%';
        }
    },

    render() {
        var t = this;
        var contain = [];

        _map(t.state.subjectModules,(itm, ind)=>{
            !itm.product||contain.push(t.isGoods(itm.product,itm.title));
            !itm.image||contain.push(<img onError={t.onError.bind(null,`img_${ind}`)}
                                        ref={`img_${ind}`}
                                        key={`img_${ind}`}
                                        src={itm.image}/>);

            !itm.description||contain.push(<Textarea disabled={true}
                                                className='txt form-textarea'
                                                key={`txt_${ind}`}
                                                defaultVal={itm.description}/>);
        })

        var subjec = [];
        if(t.state.subject){
            subjec.push(
                <img key='topimage'
                    onError={t.onError.bind(null,'tpimg',true)}
                    ref='tpimg'
                    src={t.state.subject.topImage} />
            );
            subjec.push(
                <div key='container' className='container'>
                    <div key='title' className='title'>{t.state.subject.shortTitle}</div>
                    <time>{t.date(t.state.subject.startDate)}</time>
                    <Textarea disabled={true}
                        key='txt'
                        className='txt form-textarea'
                        defaultVal={t.state.subject.description}/>
                    <img onError={t.onError.bind(null,'detailImage',true)}
                        ref='detailImage'
                        key='detailImage'
                        style={{display:t.state.subject.detailImage?'':'none'}}
                        src={t.state.subject.detailImage} />
                    {contain}
                </div>
            );

        }else{
            var porms = {
                key:'loading',
                style:{
                    width:'40%',
                    margin:'auto',
                    display:'block',
                    paddingTop:'35%'
                },
                src:'http://miz-image.b0.upaiyun.com/xplan/car/car.gif'
            }
            subjec.push(
                <img{...porms}/>
            );
        }

        return (
            <section className = 'theme' style={{minHeight:($(window).height())}}>
                {subjec}
            </section>
        );
    }
});

module.exports = Theme;
