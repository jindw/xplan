var Reflux = require('reflux');

module.exports = Reflux.createActions([
    'getHomeSeckill',//首页秒杀和//爆款
    'getTopBanner',//顶部new,
    'getGroom',//推荐
    'getDetails',
    'getSecKill'//秒杀
]);