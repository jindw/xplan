/**************************************************\
                home页面-秒杀
\**************************************************/
var Action = require('./action');
var Reflux = require('reflux');
var Store = require('./store');
var React = require('react');


module.exports = React.createClass({
    mixins: [Reflux.connect(Store)],

    getInitialState() {
        Action.getSecKill();
    },
    
    toGood(){
        location.hash = `/goods/${this.state.seckill[0].id}`;
        localStorage.homeTop = $('body').scrollTop();
    },

    onError(){
        this.refs.secimg.src = 'http://miz-image.b0.upaiyun.com/xplan/car/car.gif';
    },

    render() {
        var t = this;
        var seckill = t.state.seckill[0];
        var sec = [];


        if(seckill){
          var start = new Date(seckill.startDate);
          var txt;
          if(seckill.totalStock){
            if(new Date()<start){
                if(start.getDate() === new Date().getDate()){
                    txt = `今日${start.getHours()}:${start.getMinutes()>10?start.getMinutes():'0'+start.getMinutes()}开抢`;
                }else if((start - new Date())/60/1000/60 - 24 + (new Date()).getHours() > 24){
                    txt = `${start.getMonth()+1}月${start.getDate()}日 ${start.getHours()}:${start.getMinutes()>=10?start.getMinutes():'0'+start.getMinutes()}开抢`;
                }else{
                    txt = `明日${start.getHours()}:${start.getMinutes()>10?start.getMinutes():'0'+start.getMinutes()}开抢`;
                }
            }else{
                txt = '马上抢';
            }
          }else{
              txt = '已售完';
          }

          sec.push(
            <div key='mysec' onClick={t.toGood}>
                <p><em>秒杀专场</em>{seckill.info}</p>
                <div>
                  <img onError={t.onError} ref='secimg' src={seckill.listImage}/>
                  <dl>
                    <dt>{seckill.productName}</dt>
                    <dd className='in'>{seckill.invest}</dd>
                    <dd className='price'>市场价：<span>￥{seckill.defaultPrice}</span></dd>
                    <dd><a className={seckill.totalStock?'on':''} href='javascript:;'>{txt}</a></dd>
                  </dl>
                </div>
              </div>)
        }
       	return(
       			<section style={{display:(seckill?'block':'none')}} className='seckill2'>
       				{sec}
       			</section>
       		)
    }
});