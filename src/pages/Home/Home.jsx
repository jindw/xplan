/**************************************************\
                home页面
\**************************************************/
// 系统资源
var Reflux = require('reflux');
var Store = require('./store');
var Action = require('./action');
var Modal = require('../../components/Modal');
var Util = require('../../app/util');
var SecKill = require('./secKill');
var _map = require('lodash/map');

// var Scroll = require('anima-scroll');

// 组件
var CanvasCarousel = require('../../components/CanvasCarousel');
var AddDot = require('../../components/AddDot');
var React = require('react');
var SwiperGrid = require('../../components/SwiperGrid');
// var lazyload1 = require('anima-lazyload');
var _includes = require('lodash/includes');

// var lazyload = require('jquery-lazyload');

require('jquery-image-lazyload');

var si,move = true,tt = 0,ok=false,loadover;
var HomePage = React.createClass({

    mixins: [Reflux.connect(Store)],

    getInitialState() {
        Action.getTopBanner();
        Action.getGroom();
        Action.getDetails();
        this.textIndent = 0;
        location.href = 'https://h5.mizlicai.com/feature/xplan5/over';
    },

    componentDidMount(){
        var t = this;
        tt = 0;
        __event__.setHeader.dispatch({
            title: window.__HEADER__?'米享计划':'理财购',
            backBtn: false,
            rightBtn: true
        });
        window.addEventListener('orientationchange',()=>{
            document.body.style.height = document.body.clientHeight + 'px';
            if(!t.refs['home']||!t.refs['newhome'])return;
            t.refs['home'].style.minHeight = document.body.clientHeight-45+'px';
            t.refs['newhome'].style.minHeight = document.body.clientHeight-45+'px';
        });

        // localStorage.homeTop>$('.body').scrollTop()&&(t.refs.newhome.className='loadonesec newhome');
        // !+localStorage.homeTop&&scroll(0,0);
        scroll(0,+localStorage.homeTop||1);
        $.DrLazyload();

        // var scrollEl = document.querySelector('.newhome');
        // this.scroll = new Scroll({
        //     scrollElement: scrollEl,
        //     noBounce: true,
        //     isPrevent:false,
        //     inertia:'normal',
        // }).init();
        this.setState({
            modalIsOpen:false,
            tip:{
                msg:'',
            },
        })
    },

    componentDidUpdate(){
        // this.scroll.refresh();
        var t = this;
        _map($(`#${localStorage.goodsId}`).prevAll().find('img'),(itm,ind) =>$(itm).attr('src',$(itm).data('url')));
        // lazyload('img');
        $.DrLazyload();
        // $("img").lazyload({
        //     threshold:1800,
        // });
    },

    toGoods(id,ind){//去商品
        location.hash = `/goods/${id}`;
        localStorage.homeTop = $('body').scrollTop();
        AddDot.Dot('xplan_command_key',id);
        localStorage.goodsId = `products_${id}`;
    },

    toTheme(id,ind){//去主题
        // location.href='/?payNumber=""&source=""#/products'
        // return;
        localStorage.homeTop = $('body').scrollTop();
        if(id === 27&&new Date(2017,1,6)>new Date()){
            location.href = `${location.origin}/feature/xplan5/one/117`;
        }else{
            location.hash = `/theme/${id}`;
        }
        AddDot.Dot('xplan_theme_key',id);
    },

    onError(itm,small){
        this.refs[itm].src = '//miz-image.b0.upaiyun.com/xplan/car/car.gif';
        if(small === false)return;
        this.refs[itm].style.width = '30%';
        this.refs[itm].style.marginLeft = '35%';
    },

    openModal() {
        this.setState({modalIsOpen: true});
    },

    closeModal() {
        this.setState({modalIsOpen: false});
    },

    getAlertContent(){
        var t = this;
        var tip = t.state.tip;
        return (
            <div className={"ralert myalert"}>
                <div className="title">米庄理财提示</div>
                <div className="content">{tip&&tip.msg}</div>
                <div className="footer-btn">
                    <a className="click-link tTap" href="javascript:;" onClick={t.closeModal}>确定</a>
                </div>
            </div>
        );
    },

    render() {
      return null;
        var t = this;
        var grooms = [];
        _map(t.state.groom,(itm,ind)=>{
            var _img =  <img data-lazy-src={itm.recommendImage} data-size="100%:auto" onError={t.onError.bind(null,`grooms_img_${ind}`,false)}
                    ref={`grooms_img_${ind}`}
                    alt={itm.productName}/>;
            if(navigator.userAgent.toLowerCase().match(/MicroMessenger/i)=="micromessenger"){
                _img = <img src={itm.recommendImage} alt={itm.productName} onError={t.onError.bind(null,`grooms_img_${ind}`,false)}
                    ref={`grooms_img_${ind}`}
                    alt={itm.productName} />;
            }
            grooms.push(
                <dd key={`grooms_${ind}`}>
                    <a onClick={t.toGoods.bind(null,itm.id,ind)}
                    href='javascript:;'/>
                    {_img}
                    <div>
                        <span>{itm.productName}</span>
                        <label>{itm.invest}</label>
                        <em>{`￥${itm.defaultPrice}`}</em>
                    </div>
                </dd>)
        });

        var sub = [],proId = -1;
        _map(t.state.subjects,(itm,ind)=> {
            if(!itm||_includes([22,15,28],itm.id))return;
            proId++;

            var _img =  <img data-lazy-src={itm.listImage} data-size="100%:auto" onError={t.onError.bind(null,`subimg_${ind}`)}
                    ref={`grooms_img_${ind}`}
                    alt={itm.subjectName}/>;
            if(navigator.userAgent.toLowerCase().match(/MicroMessenger/i)=="micromessenger"){
                _img = <img src={itm.listImage} alt={itm.subjectName} onError={t.onError.bind(null,`subimg_${ind}`)}
                    ref={`grooms_img_${ind}`}
                    alt={itm.productName} />;
            }

            sub.push(<div key={`tg_${ind}`}
                        className={`subject subject_${itm.products.length}`}
                        onClick={t.toTheme.bind(null,itm.id)}>
                        {_img}
                    </div>);
            if(itm.products.length){
                sub.push(<SwiperGrid themeId = {itm.id}
                        products={itm.products}
                        key={`swipergrid_${ind}`}/>);
            }
        });

        var cc = [];
        if(location.pathname !== "/feature/xplan-MSharePlan/"){
            cc.push(<CanvasCarousel key='CanvasCarousel' big='false' name='home' loading={true}
             top={t.state.top} topMax={t.state.top.length||0} dotSize='6'/>);
        }


        // var msg = '亲爱的庄主，为了给您带来更好的体验，杭州仓储在1月1日-1月7日进行升级，部分宝贝在1月8号才能正常发货，需延迟发货的商品可在宝贝详情中查看哦！';
        // <section id="myTip" onClick={()=>this.setState({showTip:true})}>
        //             <i/>
        //             <span ref='gonggao' onClick={function(){
        //                 t.setState({
        //                     modalIsOpen:true,
        //                     tip:{
        //                         msg
        //                     },
        //                 })
        //             }} data-value={msg}>
        //                 <label>{msg}</label>
        //             </span>
        //         </section>
        return (
            <section className = "newhome" ref = 'newhome'>
                {cc}
                <div className='groom forimg'>
                    <SecKill/>
                    <div className = 'top' style={{display:(grooms.length?'block':'none')}}>
                        <span>为你推荐{t.state.modalIsOpen}</span>
                        <label></label>
                        <div>
                            <dl>
                               {grooms}
                            </dl>
                        </div>
                    </div>
                    {sub}
                </div>
                <a href={location.origin+'/activity/16-11/1123/tianyi'}
                    style={{
                        background:"url(//miz-image.b0.upaiyun.com/xplan/duihuan.png)",
                        width:'100%',
                        backgroundSize:'cover',
                        display: 'block',
                        paddingBottom:'48%',
                    }}
                    ></a>
                <Modal isOpen={t.state.modalIsOpen}
                    onRequestClose={t.closeModal}
                    style={Util.customStyles}>
                {t.getAlertContent()}
                </Modal>
            </section>
        );
    }
});

module.exports = HomePage;
