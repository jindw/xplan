var Reflux = require('reflux');
var Action = require('./action');
var DB = require('../../app/db');
var _map = require('lodash/map');


var s;
var Store = Reflux.createStore({

    listenables: [Action],

    data: {},

    onGetSecKill(){
        var t = this;
        DB.Home.getSecKill({
            type:'SEC_KILL'
        }).then(data=> {
            t.data.seckill = data;
            t.trigger(t.data);
        });
    },

    onGetGroom(){
        var t = this;
        DB.Activity.getGroom().then(data=> {
            t.data.groom = data;
            t.updateComponent();
        });
    },

    onGetTopBanner(){
        var t = this,urls;
        DB.Activity.getTopBanner().then(data=> {
            _map(data,(itm,ind)=> {
                if(itm.subjectId){
                    urls = `${location.origin}${location.pathname}#/theme/${itm.subjectId}`;
                }else{
                    urls = itm.link;
                }
                t.data.top.push({url:urls,imageUrl:itm.image||'http://miz-image.b0.upaiyun.com/xplan/home_banner_0328X_02.jpg'});
            });
            t.data.topMax = data.length>5?5:data.length;
            t.data.topStatus = true;
            t.updateComponent();
        },()=>t.data.top.push({imageUrl:'http://miz-image.b0.upaiyun.com/xplan/home_banner_0328X_02.jpg'}));
    },
    onGetDetails(){
        var t = this;
        DB.Home.getDetails().then(data=> {
            t.data.subjects = data.subjects;
            t.data.products = data.products;
            t.trigger(t.data);
        });
    },

    onGetHomeSeckill(){
        var t = this,p,needid = 19;
        date = (new Date()).getDate();
        DB.Activity.getSeckill().then(data=>{
            if(data){
                t.data.secimg = 'http://miz-image.b0.upaiyun.com/xplan/2.0/sec16714.jpg';
                t.data.stockper = Math.round((data[0].quantity)/data[0].storage*100);
                t.data.productId = data[0].id;
                t.component();
                t.trigger(t.data);
            }else{
                t.data.secimg = 'http://miz-image.b0.upaiyun.com/xplan/2.0/pause.png';
                t.trigger(t.data);
            }
        });
    },

    updateComponent(options) {
        options = options || {};
        this.trigger(this.data);
    },

    getInitialState() {
        var t = this;
        this.data = {
            alist : [],
            // modalIsOpen: false,
            secimg:'',
            hot:[],
            // tip:{
            //     msg: '',
            //     type: ''
            // },
            time:['-','-','-','-','-','-'],
            returntime:false,
            stockper:0,
            hour:(new Date()).getHours(),
            now:'',
            end:false,
            per:100,
            top:new Array(),
            topIndex:0,
            topMax:null,
            topStatus:false,
            loading:0,
            groom:[],
            productId:null,
            subjects:[],
            products:[],
            isLastPage:false,
            pages:1,
            seckill:{},
            textIndent:0,
        };
        return this.data;
    }
});

module.exports = Store;