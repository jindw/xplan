/**************************************************\
                Xplan 介绍
\**************************************************/
var MizShare = require('../../../bower_components/MizShare');
var React = require('react');
var CanvasImg = require('../../components/CanvasImg');

var Introduce = React.createClass({

    componentDidMount(){
        __event__.setHeader.dispatch({
            title: '米享计划'
        });

        var shareObj = {
            sharedesc : '米享计划，理财购物两不误！',
            sharetext : '理财购物两不误',
            sharelineLink : `${location.origin}${location.pathname}#/introduce`,
            shareimgUrl : 'http://miz-image.b0.upaiyun.com/activities/mother/logo.png'
        };
        MizShare.config(shareObj);
        scroll(0,0);
    },
   
    render() {
        var t = this;
        return (
            <section>
                <CanvasImg minHeight={document.body.scrollHeight} productpicture={['http://miz-image.b0.upaiyun.com/xplan/3.0/introduce.png']} />
            </section>
            )
    }
});

module.exports = Introduce;