var Reflux = require('reflux');
var Action = require('./action');
var DB = require('../../app/db');
var Util = require('../../app/util');

var Store = Reflux.createStore({

    listenables: [Action],

    data: {},

    onGetOrderList(){
        var t = this;
        var userKey = Util.getUserKey(null,false);
        if(!userKey){
            location.hash='';
            return;
        }

        DB.Order.getList({
            userKey : userKey
        }).then(data=> {
            t.data.orderList = data || [];
            t.data.orderLength = data.length;
            t.trigger(t.data);
        },data=>{
            if(data.status === 'NOT_EXIST'||data.status==='FAILED'){
                localStorage.auth = '';
                location.href=window.__loginUrl__+location.origin+'/feature/xplan';
            }else{
                t.data.modalIsOpen = true;
                t.data.errmsg = data.errorMsg;
                t.data.canDelete = false;
                t.data.onerror = true;
                t.trigger(t.data);
            }
        });
    },

    onDeleteOrder(xNum,xdom){
        var t = this;
        var userKey = Util.getUserKey(null,false);
        if(!userKey){
            location.href=location.origin+'/feature/xplan';
            return;
        }
        t.data.modalIsOpen = false;
        t.trigger(t.data);
        DB.Order.deleteOrder({
            userKey : userKey,
            xOrderNo : xNum
        }).then(data=> {
            if(data.status === 'SUCCESS'){
                xdom.className = 'removeNow';
                xdom.style.height = xdom.scrollHeight;
                $(xdom).animate({
                    height: 0
                },200);
                t.data.orderLength--;
            }
            t.trigger(t.data);
        });
    },

    getInitialState() {
        var t = this;
        this.data = {
            orderList : [],
            modalIsOpen:false,
            errmsg:'确认删除？',
            deleteId:null,
            orderLength:null,
            canDelete:true,
            onerror:false
        };
        return this.data;
    }
});

module.exports = Store;