/**************************************************\
                home页面
\**************************************************/
// 系统资源
var Reflux = require('reflux');
var Store = require('./store');
var Action = require('./action');
var Util = require('../../app/util');
var Modal = require('../../components/Modal');
var AddDot = require('../../components/AddDot');
var React = require('react');
var _map = require('lodash/map');
var _split = require('lodash/split');


var OrderPage = React.createClass({

    mixins: [Reflux.connect(Store)],

    getInitialState() {
        Action.getOrderList();
    },

    componentDidMount(){
        __event__.setHeader.dispatch({
            title: '订单列表',
            backBtn: true,
            rightBtn: false
        });
        AddDot.Dot('myorder_key');
        localStorage.homeTop = 0;
    },


    delete(id){
        this.setState({
            deleteId:id,
            canDelete:true
        });
        this.openModal();
        AddDot.Dot('xplan_deleteorder_key');
    },

    logistics(id){
        AddDot.Dot('xplan_logistical_key');
        location.hash = `logistics/${id}`;
    },

    toDelete(){
        var xnum = this.state.deleteId;
        Action.deleteOrder(xnum,this.refs[xnum]);
    },

    openModal() {
        this.setState({modalIsOpen: true});
    },

    closeModal() {
        if(this.state.onerror){
            history.go(-1);
            return;
        }
        this.setState({modalIsOpen: false});
    },

    getAlertContent(){
        var t = this;
        return (
            <div className="ralert myalert">
                <div className="content">{t.state.errmsg}</div>
                <div className="footer-btn confirm">
                    <div style={{display:t.state.canDelete?'block':'none'}} className="click-link tTap" href="javascript:;" onClick={t.toDelete}>删除</div>
                    <div className="click-link tTap" href="javascript:;" onClick={t.closeModal}>取消</div>
                </div>
            </div>
        );
    },

    onError(itm){
        this.refs[itm].src = 'http://miz-image.b0.upaiyun.com/xplan/car/car.gif';
    },

    orderStatusMap: {
        PAY_NOT: '未支付',
        P: '处理中',
        PAY_SUCCESS: '支付成功',
        PAY_FAIL: '支付失败',
        PAY_CANCEL: '已取消',
        PAY_OVER: '超额支付',
        PAY_REFUND: '已退款',
        PAY_DELIVER:'已发货',
        PAY_SIGN:'已签收'
    },

    orderDetail(id){
        location.hash = `order_detl/${id}`;
    },

    render() {
        var t = this;
        var rlist = t.state.orderList;
        var list = [];

        _map(rlist, (itm, ind)=>{
            var tt = new Date(itm.createDate);
            var time = tt.getFullYear()+"-"+(tt.getMonth()+1)+"-"+tt.getDate();
            var attrs;
            if(typeof(itm.productAttrs) === "string"&&itm.productAttrs.split(',').length === 1){
                attrs = itm.productAttrs;
            }else{
                attrs = _split(itm.productAttrs, ',', itm.productAttrs.split(',').length-1).toString()||JSON.parse(itm.productAttrs).attrValue||JSON.parse(itm.productAttrs).attrValue1;
            }

            list.push(
                <section ref={itm.xOrderNo} className='section' key={"r_itm_" + ind}>
                    <div className="item jc-start ai-start mylist">
                        <p>{time}<em className="order-status">{t.orderStatusMap[itm.status]}</em></p>
                        <div className='order-list-detail' onClick={t.orderDetail.bind(null,itm.xOrderNo)}>
                            <em>{itm.productName}</em>
                            <div>
                                <img ref={`pro_img_${ind}`} onError={t.onError.bind(null,`pro_img_${ind}`)} src={itm.productImage}/>
                                <ul>
                                    <li><span>规格属性：</span>{attrs}</li>
                                    <li><span>购买数量：</span>x{itm.productNum}</li>
                                    <li><span>实际存入：</span>{itm.amount}元{itm.investCycle}</li>
                                    <li><span>另外享收益：</span>{itm.income}元</li>
                                </ul>
                            </div>
                        </div>
                        <div className='noold'>
                            <a href='javascript:;' style={{display:(itm.logisticsStatus?'':'none')}} onClick={t.logistics.bind(null,itm.xOrderNo)}>查看物流</a>
                            <a style={{display:(itm.status === 'PAY_NOT'?'none':'block')}} href='javascript:;' onClick={t.delete.bind(null,itm.xOrderNo)}>删除订单</a>
                        </div>
                    </div>
                </section>
            );
        });
        if(list && (!list.length||!t.state.orderLength)){
            if(t.state.orderLength === null){
                list.push(
                    <div className="nondata" key={"r_none_data"}>
                        <img src="http://miz-image.b0.upaiyun.com/xplan/car/car.gif" width="85px" height="85px"/>
                    </div>
                );
            }else{
                list.push(
                    <div className="nondata" key={"r_none_data"}>
                        <img src="http://miz-image.b0.upaiyun.com/xplan/2.0/nodata@2x.png" width="85px" height="85px"/>
                        <div className="nodata-msg">暂无数据内容</div>
                    </div>
                );
            }
        }

        return (
            <div className="flex-v jc-start mList">
                {list}
                <Modal isOpen={t.state.modalIsOpen}
                        onRequestClose={t.closeModal}
                        style={Util.customStyles}>
                    {t.getAlertContent()}
                </Modal>
            </div>
        );
    }
});

module.exports = OrderPage;
