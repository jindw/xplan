var Reflux = require('reflux');

module.exports = Reflux.createActions([
    'getOrderList', // 获取订单列表
    'deleteOrder'//删除订单
]);