#XPLAN 文档

##2.0（160801上线）

### 夏日活动(`#/summer`)

**数据来源**

> 前端模拟写死

```json
//summer.json
{
    "data": [{
        "title": "65元格瓦拉全国影院通用优惠券0元秒杀",
        "in": "5000元存2个月",
        "out": 27,
        "id": 71,
        "img": "http://miz-image.b0.upaiyun.com/xplan/3.0/summer/movie.png"
    }, {
        "title": "骆力克 智能心率蓝牙运动手环0元秒杀",
        "in": "5000元存3个月",
        "out": 78,
        "id": 73,
        "img": "http://miz-image.b0.upaiyun.com/xplan/3.0/summer/bong.png"
    }, {
        "title": "gooro智能灯迷你创意小夜灯0元秒杀",
        "in": "6000元存2个月",
        "id": 75,
        "out": 98,
        "img": "http://miz-image.b0.upaiyun.com/xplan/3.0/summer/light.png"
    }],
    "status": true
}
```

### 商品详情页(#/goods/{id})

> 获取商品信息



> 接口：`/xplan/product/{id}`

**后台数据**

```json
{
  "product": {//商品信息
    "id": 133,
    "createDate": 1469964586000,
    "modifyDate": 1470212549000,
    "productName": "日本VAPENo.1儿童电子驱蚊手表 ",
    "productType": "TYPE_ENTITY",
    "shortDescription": "专为宝宝设计的驱蚊手表，日本原装香港直发 ",
    "description": "日本原装进口VAPE驱蚊表，超可爱Hello Kitty样式，让宝宝爱不释手；强效保护，每粒驱蚊药片可实现120小时保护功效；香港仓直发。（1-3个工作日内发货）\n",
    "tag": "海外直邮",
    "saleDesc": "8000元起存免费拿，最高折合预期年化9.84% ",
    "listImage": "http://miz-static.b0.upaiyun.com/miz-image/products/rbqwsh0706_1.jpg",
    "proImage": "http://miz-static.b0.upaiyun.com/miz-image/products/rbqwsh0706_1.jpg",
    "descImage": "http://miz-static.b0.upaiyun.com/miz-image/products/rbqwsh0706_x1.png;http://miz-static.b0.upaiyun.com/miz-image/products/rbqwsh0706_x2.png;http://miz-static.b0.upaiyun.com/miz-image/products/rbqwsh0706_x3.png;http://miz-static.b0.upaiyun.com/miz-image/products/rbqwsh0706_x4.png;",
    "sort": 6,
    "startDate": 1470310122000,
    "endDate": 1501500524000,
    "shareMsg": "日本VAPENo.1儿童电子驱蚊手表 ",//分享文案
    "isRecommend": true,
    "recommendImage": "http://miz-static.b0.upaiyun.com/miz-image/products/rbqwsh0706_1.jpg",
    "invest": "8000元存6个月",
    "defaultPrice": 168,
    "activity": "SEC_KILL",
    "activityDate": 1470282538000,
    "activityInfo": "每日2轮(10点 16点)",
    "productAttrs": [//购买规格选项
              {
                "id": 631,
                "productId": 133,
                "attrName": "规格",
                "attrValue": "100日",
                "attrRow": 1,
                "attrCol": 1
              },
              {
                "id": 632,
                "productId": 133,
                "attrName": "期限",
                "attrValue": "6月期",
                "attrRow": 2,
                "attrCol": 1
              }
  		],
        "recommend": true,
        "caroImages": [
        "http://miz-static.b0.upaiyun.com/miz-image/products/rbqwsh0706_1.jpg",
        "http://miz-static.b0.upaiyun.com/miz-image/products/rbqwsh0706_2.jpg",
        "http://miz-static.b0.upaiyun.com/miz-image/products/rbqwsh0706_3.jpg"
        ]
  },
  "status": "SUCCESS",
  "errorMsg": "成功"
}
```

**选择参数**

![选择参数](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARsAAAB0CAMAAACCJmYoAAAAAXNSR0IArs4c6QAAAMZQTFRF////7e3t8PDw//Xv/v39RkZG5+fn39/f9/f35OTk+fn5zc3NpaWlXl5e/2gc/8+1jY2NxsbG/2wkm5ub4eHhsbGx/7aS/2AQ/6l89PT0/5Nck5OT+/v7hISE2tra/9nF/4dK1dXVVVVV/34+/9vK/+HRLCwsQkJC/3g0Ojo6/+fbdHR0/45U/+3l/8eq6enpampqTU1N/5lk/2UYfHx8q6ururq6/6yBysrK6+vr/3IswsLCr6+vt7e3/7KL//r3vr6+5bae4H4DGgAABvBJREFUeNrtnQ9bsjwUxqeOAAGbTS0aUGBphn8w0TSw+v6f6t1AespK4OnpetPOXRQaB9qP+2xA51oIgUAgEAgEOnyNLgroeivIUorob4K2YnChA+FvYnP2cJmrbncrqCof5UptbQWZBYLkrRilQMyRbH0Xm07+Ntfv2Kzyg6x3bCb5Qe/YqAWa0AI2wAbY/Dg2daYCm9cHY0rVNyTPIAqKPMMw+hqwSbTgMAy/OiEmf+HofB+uX/0JbBZT9X/3TTRX2FHM/TLVVMYBmaH7SU7Vuo1G45QvJyPx7rgom6pdfVlbFWWjhUuWNki1lUJslmsU9SRJ6juCbPwv2ExIRK1YU+pxRAKOSBKL9iGb4fixNhyNj4eczZgzasyuirBZ0owF6UvSIFDy2eC1SHAitjR9HtSzc9lYtuRRna8Qzsadcka95hfZyKKH8akW8w/EOJNs+YjN6PE45YPGj0/o6a6T75vVlDeNa2AjhfLdqr6c7xuTLPhFPo78CaqSlkjzSi4bmds9841LFYR1an2VDUUTJseF2PzJqXF3xlf50q3lsdlkhEMsZgz4yexLAz2HjeO3PWnA20kDl0zToF60m41FpV64SH3j+IO+iO9PV19jw/SFG8cacwUbYSLjs5zaeCf5fnWH0P0YjYbF2SCkR0jhPU6kF+mLs614ioh96FFef8OPEPV6A+EbEcDjFfuLbCiO5lbcZJONb4Q+y6mNbx6uS7D5k1PF2WgiQOSHVpwNG/R4fzNfZifkH7BxKXNVqrieXt9mstM3s0aibinfDFJM+b6xEt9g8WTBToN6hXyjy0RlcXZCvppTcVRv64oTTiI+gNNNz47n0Xs2iW1SDWvCN+LRTtmcep0tO9jw4/Otongz7CBUMKd0rPurl4N+0TeWrpEWbpKqcJAlB2l/Y4SVD3xzfDnqnD2dHV91O2jDBo3OR6VyKk0ZmsdGYZNId5KBhmwut9btHDaazxilTbbGaBWam4sG+X+418xMdNIp4RvHSDBJhrObTWWzHd+ygprTdDWY5LBZ+63YljFe+BU+aKXKhnG4D4dnFMAG2ACbg2aDc/Vb2TyeFtDlNhu5gLabZRYJ2n6MWyTm2/4+BQKBQCAQCAQCgUCgv1NFLqntO2WnXLij7BGbarnt37E5KhevAhtgA2yADbABNj+KDaZGdb/ZOEErrRJJ6z3q9nLT9rcVjp+xUcJBT9KW+p8fZAWLeE0Wc7zfvmn5dkJFj7LqCGlgy54oORMKlRzfOARZtqg2IxxTYPRF2Zkjajl0C+s63vecspjb5qd4ZSsVZjp2RZRHVN4Wue5kowUmakeiBoe7LdJFdc3E1kTh49w295eNRXWrGlK6sBn/5V1DQ7g9FcUxMrFEJdHazWdj2dIEiXJRzsYPA8M2nGY451k6kDyaFYPuo28s2o7nWI+4dxTdSAho0zVGGmNmpE+YlcOGDXpGhWh1shKrgb/ikLarsvY3p5RQ6vd6khRrgZecbCkgCppr2lyfr9UiOYWWTNhM+CYY8H6K9zexlO7Kk/eZjSjCbPp+1tTNORfDFO9Oq4X6G6T4gQgTNX9LyW4HblL4yH2D97ovVkIVabEeE/MNG41itHyW1ziPDdY8o6mQPseBF14g+2svoEHFoi5ng+fNvb6+IdY6NPXo2Z+8YqPYMm5SC8UvcD5hg+1gUZGDpmws6mHoVsKmQ3CF52SF8J024732TfvZ4Zbhp/hZd7ysWpFF880g/BxWc6+L28JzJqH8i+0ihU29pfyyq+z6CO4Z4H4K2AAbYANsgA2w+XY2slpGrfczjZSKV+U9YoOtkvpqPEYgEAgEAoFAIBAIBDpwdU7Kavhr2Jzd18rp6ub3sOmUDLgFNsAG2ACbj8Q0pMXVZHK2eGp4CzFV21QDNlwLyTO8qR5WOaQ4PmKumO8pBjavfLNhE7tyUd+MThrZtJn3Y/55NWvMzg+NDfdNllOUKfIO39Q6D5zE9c3pbPwyRdvdFTo+nTVOZ2d3LxOUHaZv5toONrXh3Qlnc3yLLh5uX7HZ+OYQ2SS+sVM2KnW8qfdpTo0e0olnRzfXb3Kqe/5wmDmV+CZIcio+Ii7VKDaZvJPN/eXTb/ANTeY6pKwufBNMFzJVyNJ2d/mmdiew3L5ik/jmAHPK1Fusos1RklNi7lWVZHMTfszm9qYj/ih7MXs7To1PDy6nTOIgjRIVYeYKNu7UP9JC9XM2eJi+fDxD2TjVOG2cnpx3Ds43yaS2zhqvDdtMfCPmoz0Kl5+yqXXF9FrjUTf57zhP55wW98348ZIPX4eWU395P9URb14/NO6e0MUjXx2Pa5cnx8AG7sOBDbABNsAG2PwkNjfn5XT5e9jcHpfVBQKBQCAQCPQd+g/vnR8NnRxH+AAAAABJRU5ErkJggg==)

> 代码

```react
//排序工作放在前台，分类按标识识别
var productAttrs = [];
_.map(t.state.product.productAttrs,function(itm,ind) {
	if(!productAttrs[itm.attrRow - 1]){
    	productAttrs[itm.attrRow - 1] = [];
     }
    productAttrs[itm.attrRow - 1].push(itm);
})
_.map(productAttrs,function(itm,ind) {
	selection.push(t.moreSelect(itm,ind));
});


///moreSelect
moreSelect(itms,inds){
  var t = this;
  var dd = [];

  var doms = document.querySelectorAll('.select .on');
  var ids = [];
  _.map(doms,function(itm,ind) {
    ids.push(itm.getAttribute('data-id'));
  });
        
  _.map(itms,function(itm,ind) {
    dd.push(<dd key={`ms_${ind}`} data-id={itm.id} className={ids[inds]?(+ids[inds] === itm.id?'on':''):(ind?'':'on')} ref={`${itm.id}_${itm.attrValue}`} onClick={t.toSelect.bind(null,`${itm.id}_${itm.attrValue}`)}>{itm.attrValue}</dd>);
  });
  return (<div key={`${itms[0].attrName}_${Math.random()}`}>
      <label>{itms[0].attrName}</label>
        <dl>
          {dd}
        </dl>
    </div>);
    },
```

**选择数量**

> 获得数量信息接口：`/xplan/product/attr`
>
> 参数：`attrIds:1,2,3`选择的项目的id
>
> 返回参数

```json
amount:8000//价格
buyLimit:5//最大购买数
income:202/额外收入
marketPrice:788//市场价
productDetailId:429//此搭配的id
stock:100//库存
```

> 接收参数处理

```react
//store.js,t=this,data=后台返回的数据，_ = lodash，num为当前选择的数量
t.data.buyLimit = _.floor(_.min([data.buyLimit, data.stock]));//取限制和库存余量的低值
t.data.num = limit?((num<limit&&num)?num:(num?limit:1)):0;
//有库存?((当前选择比限制小且不等于0)?当前选择的数:(当前选择不是0?限制的值:1))：0
```

**底部图片处理**

```javascript
products = _.split(p.descImage,';',_.split(p.descImage,';').length-1);//处理数组，由于传来的数据结尾多一个';',长度要减1
products.push('http://miz-image.b0.upaiyun.com/xplan/3.0/faq.png');//固定的图片
```

> 图片处理（canvas）

```javascript
//在图片加载完成前的代码，5张jpg组成的动画
loading(){
  var c = this.refs.myimage;
  var ctx = c.getContext("2d");
  var img = new Image;
  img.src = 'http://miz-image.b0.upaiyun.com/xplan/car/car_1.jpg';

  c.width = document.body.scrollWidth;
  c.height = this.props.minHeight||document.body.scrollHeight;
  var loadsum = 0;
  var mimg = [];
  var imgs = [];
  for (var i = 0; i <5; i++) {
    mimg[i] = new Image();
    mimg[i].src = 'http://miz-image.b0.upaiyun.com/xplan/car/car_'+(i%5+1)+'.jpg';
    mimg[i].addEventListener('load',function(){
      loadsum++;
      if(loadsum === 5){
        for (var ii = 0; ii < 5; ii++) {
          imgs.push(mimg[ii]);
        }
        var is = 0;
        if(m === false)return false;
        m = setInterval(function(){
          var left = (c.width-mimg[++is%5].width/2)/2;
          if(m){
            ctx.drawImage(mimg[is%5],left,c.height/3,img.width/2,img.height/2);
          }
        },100);
      }
    },false);
  }
  this.imgloadup();
},
 //图片正式加载
 imgloadup(){
   var t = this,h = 0,sh = 0;
   var c = t.refs.myimage;
   var ctx=c.getContext("2d");
   var img = [];
   var imgs = [];
   var l = t.props.productpicture.length,i = 0;
   var e = 0;
   for (var i = 0; i < l; i++) {
     img[i] = new Image();
     img[i].src = t.props.productpicture[i];
     img[i].addEventListener('load',function(){
       e++;
       if(e === l){
         for (var ii = 0; ii < l; ii++) {
           imgs.push(img[ii]);
           h += img[ii].height*img[0].width/img[ii].width;
         }
         imgin(l);
         window.onresize = imgin(l);
       }
     });
   }

   function imgin(len){
     clearInterval(m);
     m = false;
     c.height = h;
     c.width = imgs[0].width;
     ok = true;
     sh = 0;
     for (var i = 0; i < len; i++) {
       var mh = imgs[i].height*c.width/imgs[i].width;
       ctx.drawImage(imgs[i],0,sh,c.width,mh);

       sh += mh;
       if(i+1 === len)
         c.style.width = '100%';
     }
   }
 }
```

### 主题页面（`#/theme/{id}`）

> 接口`/xplan/subject/{id}.json`



> 主题除顶部有图+标题+简述，下面约定为3个结构的数据一起传

```react
_.map(t.state.subjectModules,function(itm, ind){
	if(itm.product){//商品
    	contain.push(
        	t.isGoods(itm.product,itm.title)
        );
}

if(itm.image){//图片
   contain.push(
       <img onError={t.onError.bind(null,`img_${ind}`)} ref={`img_${ind}`} key={`img_${ind}`} src={itm.image} />
    );
}
            
if(itm.description){//文字
     contain.push(
        <Textarea disabled={true} className='txt form-textarea' key={`txt_${ind}`} defaultVal={itm.description}/>
        );
     }
})
```

```react
isGoods(pro,title){//商品
        var t = this;
        var msg = pro.shortDescription;
        return <div key={`title_${pro.id}`} className='recommend'>
                <em>{title}</em>
                <div>
                    <a href = {`${location.origin}${location.pathname}#/goods/${pro.id}`}></a>
                    <div className='con'>
                        <img src={pro.proImage}/>
                        <div>
                            <dl>{pro.productName}</dl>
                            <dl>
                                <dt>
                                    <span>{pro.saleDesc}</span>
                                </dt>
                                <a href='javascript:;'>去购买</a>
                            </dl>
                        </div>
                    </div>
                    <dd>
                      {msg.substring(0,t.state.maxl)}
                      {msg.length>t.state.maxl?'......':''}
                      <span style={{display:(msg.length>t.state.maxl?'':'none')}}>查看详情</span>
                    </dd>
                </div>
            </div>
    },
```

> 商品效果

![shangpin](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASYAAACPCAMAAABgfoZ6AAAAAXNSR0IArs4c6QAAAWVQTFRF/////v794+Pj+/z7+fn57e3t8fHx9/f3/24k8/PzyMjI1tbWd3d3/pFZmJiYXFxcrKys6+vr/2cbRkZGj4+P6Ojo7+/v+O/o+8On/o5U/LKO+tPA+OXc17ILg4ODpaWlc3Nz/18P/pdi+6h+//r0/OHUiYmJ0NDO4bsE38NG9vX1oKCg1rVplJSUnJyc/n4+2tra9/LS3b4y//75/3czwsLBs7Oy/bua38iP/opPa2tr+vbw+8y08eq5/p1s38hUMDAv397e1bIWU1NT/O3curq64MxllnIg7OGa/aR23boa5tZ+4d3P49FwxaFG6MMJ/oVGxcXF07Io6+nexqAPz69Tt6Bm1a0E+ONR3LME/9rH69BL+vnks44W+urh/3Ms79Mu+PDt89IQ5tiPn4ZI+dbFvap5rZNS2L1++dnK389/+ohK/2ogdFYZ/62C08qvzYIP0moczbuT9uVt2NPB3asw+JhdgrXFqwAAFdhJREFUeNrtnYlD2sgXxwWFQAIxaMJNuAQkGEDkWO6CoijFAtVa8apVe1hrt7v9/f+/NzMB0eJ2t+7a7pKnQI5hZD5835s3ExymplT7pmn1KgMVk4pJxaRi+m9gWui7bCqUb2IKpU/sGpXKNzCtmM1Ox7FK5RuYbIlSKB+6VaDrnZrKxpLy9HCvKyqnKLFLNph5YgVpMjCVEgtjMOkKWk2sBrTm5588IbdwYX5o8hTD46IWcTIw9fNOjavjCoXWFgaCMT15MssjNvPzJkoBJypaymiMcgaraXIw2Z3O68T16dR1upPvdK41N2pi5r2EgKImZFnL6kBM4kQ5XSKfTrtARLY1sJLhBlMsbiJqkqemjGFBOQzcjLKea2gUJU1KCE/0rx033ncywCR552d5RU1T0+EMZQLVPClQFtEqM1MarxcwdRFGxgv3q5aJwmTvKJhmTKv8jFdRE8Wvoq6N8saMwE32IQc0sTyK8pKP8cK9OFGYNMHASGya8hI1Tccz3oaJxCcZDniBGc8wk4upaLaPwQSGyJCeDmFCXlabYExrI7lTBjCR7q2gI5gyA0w4uaTFyYpNrmvHgmL2/HDAwj/pIjUp5u1ShSfz9E2OlH0C5ydJTZuVQGCzolhJHf6q800qJhWTiknF9F/DNDMzQ1l1OotqYFY90BiLSaL0Fr2RUg1MMlp1kjQek16n8rkxo2U8JqNK6TYn3VhMFtXhbptuZiwmSSVzy6zUGEzTFhXMHUzGP8Y082CbBEzTD0/GplVMKiYV04/BZGQjlJGP3PzpLk8ehXCS4jh4oEk5mcEvLDw7O786W0iictzqfNwHu3AgTsoUBPQY6VIMK8LRWV/NtLpqslKNDHquaXaVg6fDz2ykIctyeJaHe+ZfoSZ9jM6szq7Ci5cpio4NMVFM2MJxVj4jDfAZB/wEBSfXBY4Uy7CkpcaYzLF6XGWE1VnDWtQQkclkKarmq8E2XxMZSuBrvFWpabDxb3A6LZ/RUQ3REmYovUgPMHXhLSc232VmhwZAMoBG8AHY+G1MQjgrSdmwBU4C89UML2BMEblbWMWVdUcwCT5eUZNosv4bMEVELWXlTHEeas/MPyE2z3S5QQGuy7AUZakhVbEMOBr8QNMUNfF4f5YxNsQkrs4UkQSTwHcFAGOyWsWGV6JIZUhNXeJ06Pncj1aThtgK2OBCzMLhgkYzDhPD6yy8SEtUUuRrPmYYm7oAAElglee6aFgQZgbQ2K6pK8ZpEps4pCX0I3t5HLVmG2EvwcRb4dcqZsFZM/NIlCg2gaLCNaynm/fhB2HSLB8cvPwd7OXBqxFbnh4FdeN0BAc0g/JmRkI4xXcxRnYQtmdRAAt7CaZGXFETS4vaYXTCRKkIEsxq3BdmECYmxuDjXBeriadlAUAxhfn5QtgEEbzQ/VGYoov7/4tGDwDSVmrx2QH/lMtm5VZLPzM9DtNIE7uKzz1B8YLHgX0VU+uKVtQpMkM1JfmGginOGxsQsxBLAjMLUUrHdxnQjYgwQSyLQU/KEkzhcEYvg9NpZzkjpsr9MEypxcUUUDo4SO3t1BdT784uwV61tfppzXhMTFg/fLlDNWHPU7KGWKGhkzjWOMQ0jE2yibmNmvRgPApojMjRItOA7lJA4sSYfCgeCbzWBAEq/oPVtP/584cPHw6iKc/O3t7i4v4i2LP1pG7mHky0qPsKU5x42exsGMVZI22aFXXUHUw4Nil11CBPgDpoCEURWeK7XRkw8Rmx4NPSMQj/coTEJivm2IiDyH6wmqr7dXA6sKPFo/2j/Z19ZIvrjIUag0kSIvIsW/BlrPeridLT4io/W4BIP8TUiI1GNoDFxMCLknErJcUyoJ1uLM6L4QikADXoSynBhDtCgeQVpKfjfqyaqp4t5HTbBxtbR3tHOzvPPM/AxmOyiAWZhpxQl/Fl4zf5kSjebIeZwizLGCkpyfM4nmRns5HZwu3sOXtTvhEzAibWa9GJYUFktGKN4p7Mx4w/2WBFs74PmHaru9u9HYAEgtrDTjdeTRM7ptOs79V70ehudLu3AXZ0tOPZu9/pJhdT24Mx7W4fHW18RJiO9vb29pcmFZOSbQ83NCR/1LSQ0x1svz3f2Nrf+fgRIO0svp5cp9OclZfPis2V5bPyWfOXcvPsbIUc3vekom/fnp8fbextbDz5uLH18vXrZ+1JxTR9WI2Wl9vFavkyur7cqlZbRE3NxTqE8O3zbeRvRxu935dfPnu2eDmxmJrLgKl12D47q7abZ5ftS4Kp/Oz1ywPo6LZ3dnYWX71sLr96DZh+YSwzE4np8Kxcbh6eLZebl2flZrPaOiOYll9vvQRDg7qXaAyMKD1b/DShmDSaBc00sYWV6eJCcWFhBXMowpAXKP3+8vfmy1cQlnB2uf+J0U0kpsNyGyJTE4TUuoTt5XK71cT/FjX9++vtKNLTq1evwRCkZ4ugJt30JF4yWAZC5Si42no0Wm5frjfbrXV8Zrr8OrVbRZheK5ggu1z8lLROJKaVVrlZXl9vNder7cP2+uVyqxUtkhhe70EA370s917DYGVn42hvf3GLTuonEtPaZflyuXwJt/ZZud2GML5+eYgL7u70DtAMQfMApFSvH23BmG6Lrk0mJs2KBn5XistFNH+r0UyvrJAQvtLbefVyFwYrvToMfT8eHR159vd6kZpxIjEp1oIUfLlZHJnnXunVX6EIHu15PHt7Hg9oae+BmNwO27eKuAJU0D4o3jHYfzZMZ+Byy4cjmBZ6W6mDg4Podg9l4Tt48hIwaSXNX8JUmiOWdmNMp/m5OX8QnRjSoIqBEtm4MJs7BvNJwlwBnO5A5707nSjd/jiWuWMGy3fu8r5w/mmzu78fU7m8XC63D0eOrNQJpl5vZw8ScYxpf/deTKEraLbT768UKXfab7gY7NwYEPLPBWDDHsSwCD44EESP/osbNdkc7vcfrjCQk6v0rVYFXGNlmTf8BUu7HjKRoikuj16EW6jj2ctoamtvD82FA6T9/eh9mIoBv51ynYRsiZyUD1K5tI3sjHM6O1JTzjmYHw5uAtkO0tZxx5xHanIgNARI8I7bjVeT3fCXzPw3Xs5EaopGl95t93799dcduKGp8Hsx5SoOO7VZAQ2Z3ekQdZy+IDvgc34/lk46hx7yHeJ0x44c1tCVu6Kch6PuClUKDsgQICd3MI1Xk/OvYUr/nZi2Bph++e3zr9uflvYX78fkytsAkwPEY0+UEseULWEnO6dYPK4AUQY0krSTCiJFFXNmNxaMO3CKioUCZsMHFJsSwfGB7gJCV/7KgfhdPACT4e/G9L+lpc+9z58//wq/EMHvc7pQJ0QhTPDW2zsl8AhbokR2AJPTfhdT5cI1h0DYNxW/QueRK55uXuP9XI6yIyklzDia3+oIx3WXPxJTanErGoU8HDndHtxQQlBlBGoMJifyH3+gAs3OmUMQWY4TF2QHTlZKCiaz3TGXd7ioU8eF+TqQJu4IIduPw7k/eFoxzCGlOK7MJGwHv8oHXCfmfKLzldz+eUzkwxS3jMzyYkwQwqM96OdI4rR/HyZkoJ5S+hhkdNpxUrnOKdkBpeVtCNNpINTJ510QbeeuIGbjMG5XGhsyV+woAzgFpcAvdHQoNyBqMgdCmJhziKnjMofMoXsxhXLofnNhYcEW6G+igL02+KSIzfzdmFbWU9iiqaGRMd1KNdWLVqvVaK8OtrW19Szl2VtCmDT3YZKu/XPBIhVK+9PvhzsJF/GVfMmR89sHWdAIpmLugxvpBhe6CPgd9rySA2wO1SSZ3xNM5pJ5Lliac1LfwrRm3lxzFoHNmsEQ6Idyfbuh7/p+NS3Uy61mfSvainrKl81Uqt1se3D6pHn+6fzNL++WXpyjC1Dbu0vbG+fhL/djuier7qD22RxzhlyoRL03X6H+DbIqIFS6MqC00t4Bdrjbs0O3+CF3emx3JNCT7HOdYcp0HMABKZQIuEL5gPl9pRMah8kFWPDHjOyba33XWh+O50LkVN++eVx5EKb1Zr1+uZzylKvlVKrVXB/F9AvC9PHjxvbS0u7bNyzzVzE9jo1TU99lC4GEANPmmj0BmB4UmwBTc73errbPPJCGp1LVdc9dTB8JpqUXCJNl5l+DaS0Q2sRqSufsyuHvx9Tr1WFw26vDrdfzQBjy1BVMX14ApncvNm4wxX9uTHbkbxp0VywhTM6+vZ8L4QiFfbHo/P6e7nB5eW1tGa1SpNyvHU4TTE8JJhKbMCZv5GdXUydU6vRLaVBTyL5Wyl2HSGxK9xcepqaRi72jF32HmN4MQ/jS29+8kaRuWjP182JK9/tpQ+C4nzCYbcebtuJC0Y4xldaCa/3OAzBNn63ftfaCgukNjk1vz4eYOIRp6udVU6LU38yVQqeakqHUR8QM/bU+JAShkNmQLq1tPiSEV6uedrueatd76/X6esqjhPBpgukdkRPB1IjcumJwC9NpZewcWtDgfjxM9pXTUOm6Anjs/U6nf51bWDje7PSPnWl01hkKPADTGWRMZ6loy9Nr1aPVVjQ16OmevnnxCwpOb8/P3wKlpbdvvgAmzdS4wQqZPfL7Ty7wmOv0vTNhRqlkruMMFPFc05w/gGcCAiTVdkBxFx7J3ORGxyj/fmhP988MVhbq0bKnXPdEq57eei911h5gAjW9wJiWXmxvIzEtnb95SiNM45zuZnYIELjmEhU8Ryhdp91FM5mcKwGuEozzTj+gWRM0toPnXBjIyG6uhAfCCBM8SBcOB2TnQSh2jJ964jdA/e68318qVhJpF2Uzhx4bE0oBPNjqHrI1UBPBhOS0PVBT7R5Mw9mh0pzf4AwMJrM/QDOLZsimQ+ZOyG1AinMRNeHJAtxaZWRny4cQJlc6RNkTCNNxTio68BxC7hjN89lgrCgVLwK35ssfbei7tvyVaRQ1vSVOd36+QZzut6eRml7zjRCO1EQwuQwVDMZQgSGe3YwGtsdSwOVOO/Id4nRO180A2HnlBlTONBnpOpTIFhgMetLHpTxS5fu8zV5x3Zo9fpwZAs34lGCACbxud3uXYHp+DyZbJ2/G1jGDa4RcKEzNnYzMnLnT4FtOh9vhDoyqiXIrTndycWFIA6a0MvOtYAqdKKGraAYFmWFI7QbnDbrvTBA8AqaVVvWurZPPEDz/NMC0tEswvQFMxrGYBqHXFQj6/eZSgAQrW4JcFnDgMxW7ggmrScEE8Z6oyQ5n3YkrpR6CqTSY4nV3KqdUMH1MBfOoXwjmrucMo/P+uX98Lnyh3m55PGdbnhEjIVz32wuC6Z2C6fzF04jWOK6nA0zBBApNZHYRNRxjIvSwD+Iz782niprcCdzT3WCCwk7HhWMUU3AwQ+BKI1EFN9GEA3SjrsqFWXIFRlpqS/8lTLnvwdRseeqXVU+q5YlG6yiBGvR0n3BCgL0OY3rxG8E0Vk0oqAYVTGh7LCbnyftACforvz8PwR02SRYBd/lTVBjdTgNugsmVwN3cezK7BxuJY2kzgGoN3cVEuYPmP22bJeq78iaPp9U+89TXYexbh7SgqmCyffoE6eUZltPS0rt37758eYqnwjX3qcl8AcpwO0IfQl87HXJEe/59Jwedns1Wud4cJBE3anLj3s6NkgCEyY5wJtwJp3sObbmoHNC1IZejipV04uJRr/ou1D1fmYLpC3BB9uXTp09fnj5//vzpH2AiV3jTF6Wriv2auqumU4ehdJx3FqniprMCEI8rNvucMvc/ggm6u7kr50/5UYvDr/IBMkMwbUNcEBxE68sX2HqevH1hRf34PBQ0Pq89r2lrQOjLFxqhqj1nbl8xUDGBzCQhGYkwyWSSiRCjafrWx51VTAjTtF6o1bRgtSSjWFKrn1Ex3eE0Y9Qjs+osgoBwCYLlNiUVExnHKJ+EHtY4M61RMX3z3+3VNVLUpWRUTCqmR8CkebDNTAAm1VRMKiYVk4pJxaRiUjH9aDOiZasoJvZHZQRWJ7BC1mv8STFxPhMyHzN4tYMTupjyfDo7aIrJB8UiWQmtHecLZ00FX8FnAgQNr55KsmglIeZWbcOX583G47wpHvfWYmgFRxrRgCoKNFXDYJiYILKcKEYY+FtcZMBU8kYoo9ztctbHwVQTfT5aomhfQdZRUlJm0XNiPh96UWQ5z9hXmAabUow03RQTWG1MR+nkGrSRZeA0l2EF3F4T4uVjh5KJjVnDk5zKZkVSm8iwWq+WrKyGMMm1pGyhGlnKIguDwrU4QGXpR1MTraUYUasVGSPLUXSYhQYZ45xRMo5iisGLLxQQkwY6kuQJJu3QEQQ204j5CqiRXcCUKWA1CTpghZiipo3FRGclbQyrqUFZuEFtOpb2Zn3472V5n0mOdzmTiRfgvTT5whFUj5SFaiSZy4qy9bGcThBrXdlIRdCaZhxgqoXJ6Vtq0sVN2cHH3jM+vB6XxCnvv2wVZHAsAgKpKcNlM2yNFW6paZzTaXGlg9iEikMJTsdCyBqqV6plGdAXWc2WiTGmuJVisnpvWDR5a8ZHi01SltdzXnixokAw0Ty8hcnbscnopdms0kIj68WiYvB6sIqaaK+EQBRoHc/zGY6NYEwjahpvTMxrYuPxuMhrEWG0XCoNmCJxPYQ7IAbS9YKyZA5OR+JGKtNg4lneQpvELrxNbDZMlhL95zHp4uD4aKlXRrQomEw1KoOW0rtRkz7WFVgLR15SUhZwexjONOAIB+IWhkMKxGqSeV3DZBpVE10AhxFgv0B/1ZER5eqzcBpKIzUJsZoQMwIxEj4bUpfXS9matxGvMTGp6zV2aYEVw+Fkhn4cNWlFtKJwRpaoCK84HQ07WpDWDSaL3EXN0cmo+zbGu1SGG/nv2xjGJAuZDGwTTCI90MbAeaDFUIgd1jpQkxc6MqQmmoo0BqXhmXEmAoggbFNSJlyjQKnIO/WxmISVCcKNWyBGZv6uNOEbmCQ5Q96wmpFFLxNhEsI1KcPrbzAZ45EYUoXIxDPgo6B9vTyyKiBHc92wyWuNaVFhlBCgNYmhsUkUukgojt3BJODliKF7ywzUZPQKWjYpgK8mdbzI6jgGa8zI+kTRJEOsQ14Z1w4dGDoZi3jj9v8sJmO4AEZLtM8XsyqYqIjJh+PUuLwJIpkOi7Ax1FPXG4+wOikL77dOxn5Joz4PerpRNd12uogXP13PagdqYjI1WQsdvdEiR4BmF3xOikNZrZzRyVrWa0ErSDODeK/1ikwkzIWFWPKHp5fj8qasl7yBFrkxjG7hcI3VNThjxGT62glG8q0Ra5C6If0ZxiZUk2zCqxuzAh23ghbhVEOueaFWKRJORlCYJ5i6fFLLNyBc+WT9j8b0OIYwcSa07qc6plOHviomFZMK5rbpx2HSqJjuqolSv1HsT9j4L16zWlUyoyZZxn8ppE6vshkxi/GerxhVOY1qySjd94W1RotVrxr+vKSVmpm5/+uPp2dUA5vWqN8SrX6ZtopJxaRiUjGpmFRTMamYVEwqJhXTfwnT/wG5Ko9JvTgjOwAAAABJRU5ErkJggg==)

> 页面标题是后台传的

```react
//componentDidUpdate
__event__.setHeader.dispatch({
      title: (t.state.subject&&t.state.subject.subjectName)?		     t.state.subject.subjectName:'主题推荐'
});
```

### 首页跳转记住位置

> 在需要记住进度的地方使用`localStorage`

```javascript
localStorage.homeTop = $('body').scrollTop();
localStorage.pages = this.state.pages;//页数
```

> 进入首页

```javascript
//componentDidMount
localStorage.homeTop>$('.body').scrollTop()&&(t.refs.newhome.className='loadonesec newhome');//需要滚动，添加遮罩
!+localStorage.homeTop&&scroll(0,0);//至顶部
```

```css
.loadonesec{
  opacity:0;
  animation:anewhome 1s;
  animation-fill-mode: forwards;
}
@keyframes anewhome{
    0%{opacity:0;}
    50%{opacity:0.1;}
    100%{opacity: 1;}
}
```

> 需要滚动执行

```javascript
//componentDidUpdate
if (+localStorage.homeTop>$('.body').scrollTop()&&(+localStorage.homeTop<$('.body')[0].scrollHeight)) {
  $('body').scrollTop(localStorage.homeTop);
  t.refs.newhome.className='newhome';
}
```



## 2.1（160809上线）

### 秒杀位（首页）

> 效果

![秒杀](data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wgARCACZASoDASIAAhEBAxEB/8QAGgABAAIDAQAAAAAAAAAAAAAAAAEDBAUGAv/EABoBAQADAQEBAAAAAAAAAAAAAAABAgMEBQb/2gAMAwEAAhADEAAAAe+SITASISISISISISISISISISISISISISISISISAMbGzqct8nS7unXDH1268Guja+TQdBXeaX3s6jH9ZPk1+w83mF5zZMS+70a/F2kk31WgAAAAAAEEkJEJEJEJEJHn1Ehg1GzaPLNi0g3bRXG3a3LL2B6Myeck6JzecbZoYN+AACJiTX+Z5u1+q9cfl56dLXy3qzoPXOzO/R7DkOvjCJiWFPijyX26u42HnU7wo92DEvsFHjKGuyMkYll4opzQAABExJreY6fSurznVel/dPiuqrxPuvTsOh5bqb8ETEzhr2XVW2JkWirNp8GSxLi1jE5LHyJqEwAAAABExJrtLu9Bj1bmq7XaRsdTXfM4tuznG+v6nU7bTCJib44dWxZ6a+c4nXes+YYPjYpY3nLTXFyiahaAAAAAImJNfrowI7dthYsTTItqyFcnEqS2G65rpYxiYlXC8U6zn6dv70vqt+hs5qTZ2agbKNcOmcyOmcyOmcyOmcyOmaPebc4aZgRMSc/quoxJ7Oez95SrgRspZ6DE66uN9H1us2ccsTEzTmdLutL4v0AY9ACyvNtSiNhVpnhetr5munGHSABmdpxfaen44dvngeZkQkQkQkQkQkefQczpe0w/O9bl3UMtuXdQOXdQOd9dAmnOuiJ5d1CLcu6gcu6gaPtNdse3zg6eSFIuUi5SLlIuUi5SLlIuUi5SLlIuUi5SLlIuUi5SLlIuUi5SLlI//8QALBAAAgIBAwEGBgMBAAAAAAAAAQIAAxEEEhMVEBQgITFBBSMwMjNAIjRQJP/aAAgBAQABBQL/ACcj/Ft82apdg9Li/fk3qT9+q5ytb2PCu9am3038pvUvwL5XF7AyWsyhncq9liNfZwaZrC25xqqbmaG29mQnk+u6bxxu3Y1eWFaiCoBuNSq1KqNQDEUIj012RdNWtQoUMumrWDSooWvaxprKcSxawk2jfwVzhXKptb9DznnPOec855zznnPOec8/r5x9T3+mfSahdxSvZqH5Ddu2NZbihrltqO/JZq70ubajctVR/wCWvye9G5MGpKWG68KZbWq0Jsa7xe8t1QqZNYLDzDPLOWd4UTvqw6xRF1Sv2H0j1iyChFZtNSztUjpsXfxIGspSwhFE7tVsVFWcS7bKeQtpFeV0iuGjMevebKxYBpwPH7ywZfNtrGq4GvS3vNlu+sMACIo/lS262H0moveplutNh1Lb7LzWmm1bWH9P3jfkrzK9u1bxU9msrdacOFduWux91dGwQ+k1Gm5ytDB+6Zuak8a1Mr/p+8tJB0xAVF5brtITK9I25y1MN1+a7LcV23PZD6R32w3rg3AMLQWzbHtNarqFZlfdOUbrLdjV28h+r7x/yaao3VlNob5TbllgLFRaFx8yl2XsPpLkLNssVeNgNjQ0rjZuKoWeunz2/N4/mVff9X3j/krdlCnlW1cHlzKSucbSGVoHVuw+nb7zOf1PeP8AfY1CEIDNr2lFXbSoJoqSwPTfTbW+5YfSXiVKN207WH864Jbv3279rF+WvPN9b3mutNa8Kxh3W6rUHvCDCUDM7zwXP8QOzTtuWH0ms1PDBryB39ieoPnqNk6lZOpWTqVk6lZOpWTqVk6lZOpWTqVk6lZOpWTqVk6lZKNc1lvh95r6uROYPQN2pvNZRghxWwqOpFd8Sl1lTHlh9J8S/N4lQtGrZF2HjCkq9bp49L/a8PvHoZnfQC092XHdDO6md2aLRiHSzup7D6T4l+bxU/aVU0IQNJpn+Xafl+LS/wBrw++RMiZEyJkTImRMiZEyJkQkdnxL83j5bMc90NtjQ2WMPFpf7X6d2mS+dOpnTqZ06mdOpnTqZ06mdOpnTqZ06mdOpnTqZ06mdOpnTqZ06mdOpnTqZVoqqn7ciZEyJkTImRMiZEyJkTImRMiZEyJkTImRMiZEyJkTImRMiZEyJkTImRMiZEyJkTImRMiZH+P/AP/EACgRAAICAQIFBAIDAAAAAAAAAAECAAMREhQEEBMhMCAxQEEFMjNQgf/aAAgBAwEBPwH4iPge/wDTrUSc5jIFGTFryROgkvrCHA89TY7R9DOA8NdYfCe8K9peDqyeQRjNDQITCjD3mhs4hBU4Pgrz9RR37zGg5hvlzajnkLSBidUzrGdUzWYxyfBUgKhmjqK+4+4rEoCZVRaRkjOYQQAG5JwlrrqE2N02N02N02N02N02N02N02N0tqao4b0V8QoUK0uuWwYEV1AxBxeE0iO2rlwv8K83OBBacf7Os2vTj0fkf3Hio4ytKwpm/pm/pm/pm84ebyib+mb+mb+mcXctr5X4v//EACcRAAEEAQIGAQUAAAAAAAAAAAEAAgMRFBITBBAgITAxQCIyM1BR/9oACAECAQE/AfiObf6exXpfVSCr4FUEfXZXQ7reFIO1ctQC1Bagg4H0tYQN9x4JpdsWmu1CwrNqWHcN2mNLe3Ixgm1thbYW2FpCAoeAxtJ1FH+I1avuj75P4mNporLiWXEsuJZcSy4llxLLiWXEo5GyC29F9A5cR+U82iytsWtoabvo4L7D4puGe55IWHIsORYcixplizLDkWHIsORcNE6NtO+L/8QAOxAAAQMCAwUCCwgCAwAAAAAAAQACESExAxIyIkFRYXETIAQjMDNAUpGhosHREEJgYnKBkuGx8VDS8v/aAAgBAQAGPwL8HNZabrE2cuW32AM2tnhY+1Gu804qIx44ZmQgAHBrqGP9FPaXuo2vL4QsPYw8VuUVmp9ya6MsiydlY8xFv6cny3EnMBqv71h7GM2v3nzu6p+R5eAPVsU0HEqLbJGZPjZhYIz5S5uYuATIZJe24mnsCOcvoN//AJCDZ2S0mIWZ2fabLRAhZgHt/KAfm1PYTMVHoHzUPxJbwA+zNJBiKI3k75RMyIjLuCyxQWRZcfmVCWnkg0WC2mNJ4kIsFJMkiiDpfTi8lfejgXEj2LxZLP0wjFiLJrYo20FeyOSpKzbxRGhrzRNapxmSfwJhjI19bO6IeKw8PZOj/SwyzGeW13CvuWG/tC5jqElNz4vZnFdMl0QF4O4l8E17OfksJrhjOo6jX5TymoT3y7I0DM2dyDWDO4yau3Spq3d0Ti4kxmqeqruwwnOrqAGzu/ihLobnipyUjjCEPb5zdjk7+Ck+CSSYzENWUeDZW1vl+qbsM23TJbUeRghQGqFZWR5LSVaVQdwTNOBhZto0iriUHHDbI5IsLRlKzRWIWaKzKBdmkeq4j/CNL3TWhsBtoJC2RFIWX7szHFa3NpBjeqvd7G/RXnqAj4x4abtom7bgB90b1cjmFs4jgJmIH08i5OyuytFgEPGGq88faix2K4HdzTgTJzKCfaFvoFSadxuVoO8yUNhsExqt7k1pc6vDwdyZD7+uAD/kJoc9len/AG9GcnbNCeK2UJkZlQOm0QsXhKq2m4rJ2YA6Imzjz7gOzTi2VnJEuo4fRDXkG/tnSsrHGPzEn3oHZPK0dPRnQJRc6eteKc5s5VsYkcV43Fkck8YXrI8I4L+kOG+ncAgk8ApAJETTcogxaVEGJiVoZ/L+lJZ7CgAPiH1VGmOKts2zIDZr6zoRGzT1XT6A5ayG5jRdm2B1Q2plA+5YkUEp0VB5K2zwhZSJnh3BQuH5TCIiczY6I4cUJnMgwt2WumeKLuyFxTLuTC1pYA7cPenHO/hUIDJlgQ4zqVtnLCGUloaE4y483CPQHIwTcqohyBduVbJ9y2UDYFSqeQp6MVtkAlSHGvAp2Z1WmEYr+yxCRvTpTiGh7Tatl/XcaYr0RmlOEfILtJ6VP1RrwgJ3WyNZVhtUN0JDbjeqhhO4H5IHZmTPH0F9YLqNQDIcJguMFYbs9DQrFwzEyisUc0W5C5sTZEnAcHBOpDh3AzIHTxUDCw46Keyw5U9myVoYtDVoatDVoatDVoatDVoatDVoatDVoatDUGOaK8PIOjU2oTS/EyPmTAusNrtpoqViEsEF0yqMNU/PSSszMXK63VHEdiB4AteVlDAN5juN/T36R+7oUmI5GUX7phF24XQzCJ7+H18gSMUjlCGd+aOSgUXnfhXnfhC88f4qrp/ZbL8o6Lzvw9xv6e+6gh1NYCIiMlRtgp0tDtrenX1DZ3BYgbE5q3+ffw+vfurq6urq6urq6ur/AGt/T5ADO6BzXnX/AMlV7j1Khz3Ecz38Pr6IM004LU9anrU9anrU9anrU9anrU9anrU9anrU9anrU9anrU9ZxmJ59y6urq6urq6urq6urq6urq6urq6urq6urq6urq6urq6urq6urq6v/wAP/8QAKhAAAgEDAwEJAQEBAQAAAAAAAREAITFRQWGRcRAggaGxwdHw8TBAUOH/2gAIAQEAAT8hQxEMRDEQxEMRDEQxEMRDEQxEMRDEQxEMRDEQxEMRDEQxEMRDEQxEMRDEQxEMRDEQxEMRDEQxEMRDEQxEMRDEQxEMdy5UU/4pFmQOoYxDTB1xJ085UB2iYBDIBI2FgOq1gXdcpBpFRA0o6mslpJrWEFTJqIywPWiLGsocLyDTc8gBLAEj6UpFQo0gllEGWhwiCBEG8uoUFa5gFQEkCVzVo8IJAtsNEBInWFMis5JiiRZI+6QJlRARDRTMe+XCHRZC1hBiFGKt4kBpgAlQPMCQBPSgR8zUiQ1joq8w5AiAIroxBNj+f4AJJIILAaGeUUH17AjEWTJqs9JqABEquY+giSARtT1hCl1Co6GLw8sir98YSd/ApQ5qD0jV0oOHyV1KkjmBzIAQkT4KA8w2EFtzHYAIgvPoVAIXFoDfhAGHSDwRoQoOgjxFYThVAABMMQ0CVhHm4TgtIgdvogFQCy8oYGPCE+QUeV8Rw0Guw/wEloCPDzjw848POPDzjw848POPDzjw848POPDzjw84yNPP+xC4gaV/p7f7Gp216XbGax9c26jZAGX3VclDV7yjqDRDRS7Ugzl6LmNasHuSAeu8eEhJaLHgEf0GkZFr1ENCC3KCZIyjGlkkyBULVIqHQFKKyrel4E4bIJUlu5iUalBfogMWnDaXI8yMDArG6rMpZ3iyICtDCGNSge/t3/b2G5p3aEdQ+rmkVw593Pu4RCKyrKmlmWouiZwwe4V7F2H+iKI1iDUesLn7GvWH5iMgUcpLVJwIFi9vNyFEKGolfNDpCoAGJLUSSazg8QXEtggOihWFioA9VfGDdVJrRio9Icd85NQ1sJo+AEYEEJIKC76PzjESN0F1UcWnbIDWoqDCpZblHm3f9vYAA2hMyEIiFGudUSVBdGAOpQgESVdBTYEZ6ShBCq/OEaIAC5Shet4NFyiwu4CERWg0VrGCwtyuSlzAKjZkU+bcWHRV48D6MN79DngvT/J7ewSDuhI9IIPeQSg1jEalKxsQBcaKD1SK3RvBzxxppDCCQugw2sBuPc4MdqTUe4BPEUKqPWExoSDAA3XfrtCyJAJTZeDpzH2vkUPUmVD5IeoP8l7ewxsYlyQTgFEGiCVTmVpgBeKGNtpGpiQ0QWapCkF7G9IEBtWvP/yAAiuzR49wCbOZ2PGABUZIWbypCBBDYEwNrRBpJhZRHXAqWScCfUqVEh3Yf6GaR94aWyWegOJiuJaHpNpiqHp/b29hBGD7QYIHMW9ZRPcsImwl01lChW7RviDSHprGvNMEudOJjUr3Saf+wqkSAkF7dO4ENaDRp8iCuuNwRVFs49QUFPiBVaB3gK6X1lD0FVxNpUlAChobF6w48AjD8oQFISPY94oXwql7RiBHQRq+vSAaZIDqWw/t7ewOSKYWhu5UslkwjVqRLrvAFTJQKMJGg5pCijGhpBXpWAaFTr3QEEMFiMUOuIw0w7wAAJAg6j/J7ezzcFyaIzXBSFIJYUvACJAPpC0QgPelbQgQ1g5lBp1CPuAzQllqpwfSU6YUJgHWFYBp0FqeYIBlghZcOBooHGEJlaR4hAItKDPygkaiQjz0gxkcIqiYLXBAGjrt/f29glqAIOlYaurSaBPtAO1uAAHEBGFVvE7aAwD7ZWIACIZHSGWwTFgd5Q50xB7gwJIzphICK4F0IkyBYkVnnARgFYXM+qZ9Uz6pn1TPqmfVM+qZ9Uz6pn1TPqmfVM+qYN3FL7z29juJJqMAVQqADWr4R+apYCB6joJQrkC8BM6hcD3hAp02XpElANdBvBFrxYYOHoE3H19w+lue+xYVg9UHKwUCH0QGdCdSBkLLdYFI6GP6x7ewCROEw0dcGGvLEKWgBq4QBscPxEQr70EuApKoDAItruzuH0tz36BkoNVHMe4AkgdJOqEO9A0bG0CEowobh3cc3gyshvl0/rDArOk2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HMQuOz6W5/h0Jgek/RQQh16pm2iCn/AFw7KNhKfsj4n7I+J+yPifsj4n7I+J+yPifsj4n7I+J+yPifsj4n7I+J+yPifsj4n7I+J+yPifsj4n7I+IBlAs3c2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HM2HP8Ax//aAAwDAQACAAMAAAAQ88888888888888888888A40Iog0wYEAU8888888EMMMMM0w044ww8w0888Usoer+U04sMs888s888Ugle6cU11xx41888888UzQNxcUrMPMuf888888U1KMg+UqaOe+++++388UeWF7qU/8AffdOfffffPLDDDDDHPPPPHzvPPPvDDDDDDDDDDDDDDDDDDDP/8QAKREAAgEDAgUEAgMAAAAAAAAAAREAITFhQZEQUXGxwTCh0fAgQFCB4f/aAAgBAwEBPxCL9MAEi68/ilYf4YGscl5cBgUMTiFqXyxpEmo7wT9cGdDKzAo0E71BItaABerTxCAiQKrWDAVk8AAIEpNQkAEzkfMtqAwAKma4PQAgHRXiEKn1gIEgdgzAARHgCqEOvN8/usppD/Un9piUa98KXwKP7ooBbSreg4gAv2uX25wQDsMX3vL2EUqhVLuerEu8Yr0r04CQaHMxDeYhvMQ3mIbzEN5iG8xDeYhvE3R/AWZQLYTtUV0MEAIAUCp4gMlNoQ1v+u6hUJPD2nErxf7kQl9QApyfU6ZlZYB+Nr5ds/h7PyfSdSxiZDtMh2mQ7QkuPaAaQtiZDtMh2mQ7QasgL9X/xAApEQACAQMCBQQCAwAAAAAAAAABEQAhYZExQRAwUaHRIECB8FBxscHx/9oACAECAQE/EPaHIsN+Ppp+HIet+5qpPbpZxCK68BHPCivHvQE2SFkYEGOBORMrpwiBJ2sZqyEIJekGOxyFIBwAdIwKBU6xEJBfx5pSBa2NuBA1ZWb/AM1U6X2rmmT9zCaUjkJoCUvjWEAQm3xABIdIa9N4dY4EZqi0vHEvHEvHEvHEvHEvHEvHEvHEaCnoAKMMOMNwrUCsZV4d7xGowgTZP7TraXcvPj47ejvP65Se0ZZGZZGZZGYB6HvCTfvLIzLIzLIzDDUJ9r//xAAqEAEAAQMDAwQCAwEBAQAAAAABEQAhMUFRYXGR8BCBobEg0TBAweFQYP/aAAgBAQABPxDjdq43auN2rjdq43auN2rjdq43auN2rjdq43auN2rjdq43auN2rjdq43auN2rjdq43auN2rjdq43auN2rjdq43auN2rjdq43auN2rjdq43auN2rjdq43auN2rjdq43b8JEF5DfD/4ovR5UKBYPMFSSYoqEDfZNOzRIYaZCa1ZBlI4WF0zNTGpohBQTwZtoX4p4wkxIAkNhbiSXRoAUEgkLI5OBEXLVG6JZuRkAmYjONKPGVXMRK2GQCG6XSjCIoeQxgYLexVwvLS5gJ0W4vQJpO2FACJVdQo2F81ZxGGpI1Mg40zSZv4BiQzuBKkF5pO2oLSJJkJjfF1AxjZ5oRhvZAvGc0QVECkWAglk200qxqDxEAgm8qTGM1P4oKhWYkyRu5o1JEgRDK5ctjilIYZSRDDlMkN0yFHFPKQky3Sb7hbNAQRMgMCQwBld/6AYiHYS1KbQSXD01LJ0j0ClwPUS2ItpRutTJm2oOCA0pUwqzbyACzBk1PL1C6skjfI0tE+QNZm6k3GZPNXNYyJS1hFiyZgxFWCBmSwEXpYigD2kIcc2oQpiDkRkgxoEaRSy7AbSjDGul6Dbc49bxK+FBFyKjLlGXsAkMGaj+I46iBnpB7FQl/YNiIkIRm99ajQEBwBycLhVkkgLZGA9pY4aQc5oNkikY0Uq2BKwRlBULFwFIZGUQCMotuHrNTjsEiICA+2d1/oCvFpzFcHx0rg+OlcHx0rg+OlcHx0rg+OlcHx0rg+OlcHx0rg+OlcHx0pNKYn+YUJiglErg/k1eM/yZup9+me21k9Tc9qtCBL1uZwe9K8EmKF0YRtIzpGVgXjiAuEAywm5NQCBhLNAK2sEbqnfj1QUhkITtUWeNyGYriLVW97zQycdiRk3bhK6k5tRO8ajSYJqAB2p5+5gkChRG43irUI1SgSXgKQ5hkVBBnNZXfXEzhAWVCS/6KxjMwJJVX1S6BzQkVhKBdcQY1dL1ISSQhEhWzGJNprIgjmtBFUMxcZsBUgOhxtKQbwgltKNI/LV4z6PnCIgT3bTTQqCyEEbpihkCklynrIQHwNQkRWE1H3Qm+pgUJMl/TN1Pv0fv5mFUTKHXepgSS4ITZu2lTPOy95GReItWtOExMzJDNi9AjM+U3UUDBg7FWi7yRXhiYufvNQtADMMSSCljO1L+Vp0iCZWbL11om28hqsoEg7TGKfmCIOEpxMTOuaUQZuISqJXiUx7YtTxExnv3SJlvBvmpaFp00ZL3uDK81PexYSSZ0MUfuS9KFieNVopRQDCcMklWIYShKDGXzWBgDrEPNCoKYlBkunyt+erxn0JOUknsUuWzEiHbeial4IDfJppTLbhJpTvAbcsONNav7s5vY69aGAKZQlZuLMLOkUqdxDcNzIa/7SowjL5Ta98Hpm6n36N3xlkKzpTNsc0eu2Nx7EJGC06l71Ni8SUGkOwU6y0YTTljtfF5tpRgMkBInHmf6mrxn0U04A2sqFkyi0QwL0pJRjkZDQfMBKQLUBzjFI7pohJH/FnXFIF0CNwg7JSDAWVMzMLvGsTFMghKglhxlm2/FTeI0AuW635oxWbqffpEAs3FRwwAw4mQvRQfuQgJCywl2GggItYHaBErGbimgFSZNWSRNhofUlTNC7xmUXleY1hBj+pq8Z9IGykewoF5T7xSgbOccUjRQXQiQgN9HtTY9PJJsY0aMukEIQLXtEVY8JEgEM553o5dQkwMEmN1txREGaUCLcBdMOaMWBkQN5a8UYrN1Pv0FhcBJhEtwams3qGDIi7hkjo2JbUkAaYtkDedTAl6EBKcIsyF50dItUpIlZSpaQNjU2JFzMWp+i8Ee6gT2Kk4cJhOmT8ihRJ8CEaLgbVh5iJRFplLf4rIWKuqZtMIbfP82rxn0UZuE9lHCTEJiz+xpRlowgsmR1n2+mkuAkKAmABOojadqCCTI4QxomlA7wWQjGIZTNFqHA5XVlE5UyiIJFGoFrEYdd6LudAIy3Dg7MUYrN1Pv0TZviGovNwiSJ9mnUXIoIS8paEwOG1KmkVkFwkzOiCLlTdyIS4AJKdUmmtNawTCEJYE3uxmKQNMgEG8disFn3TRYQoxFy5QnaKV26QvtDKlLLg3iiBkxv8AmY0pdKUGyQwRbEO8Ul7Z5RRkBcLl41z/ADavGfQBAuiexVqxh4Fxe0T8UIvnsTBMX31qAghCZLJKcYtFTDBrL/DpWbjI2YDNJflWje0ln7qUhIxqcG7TOXKRN18sBEFGKzdT79ME0LMhIjIlaatm68b0nIULJvG9F1aRJE/qavGfT476Klz4JWV3g+6QikVYNkaJNISyzDWWulVaaRNJEWFgLGy/dY3EUQ6KggvRCERYEWGXE1rKhgQe3pm6n36PTE75bFsb4UsXIZDEywsY3iKTcmyaHALGbOKZUiRMiEVu6ues0QqxAMIcm09uKIEiFLaS1u/vS3AAYxL5OstLDRnG2GzSyiWUsQb2bzLGlMQQJaK+cXstjJ/Pq8Z9De8qgkS4tNWfhNEiLiRcJaKLowQRZhy+0UR2tApjE4j5pJATBme/NKKYHwou+4ULZbUzNDyZJSZTqMYiLUqkMdEiJIS2KMVm6n36SgMUJBMFtcNZNGgHUTes0I1R0ZpcpxhSGdZ5e9SN0ZYBLvmvD/avD/avD/avD/avD/avD/avD/avD/avD/avD/avD/avD/avD/aluhBmRi2Zn8tXjPogavWSBPxNZ42iwzASss6XYoPKaCoXQJkyUlRMIUaTetIAAifap15j5Ay7VHHMsDIiFGJOqEYEjTajSWLDFunC1FZup9/wsHLVnxUiaEduCSmS5oOw9ktyT2qRmwkLaPqgcsyxc/x4b/n8H+Wrxn0aIIwAWjL0pbyVoTG0mlMwWSFBixijUIdv+NJiFMJ+qlct1/TTuOrZQO1JLdSYlcst61+5CGXtRms3U+/4WCnS0w4ZbTz0qBtKWARhcYolNHODK9w/NLDGTwSFpJnKfNDMkGoOSZYvgn83wf5NoDk8/wAkpS1rWta1pERmTXn+JgEJQYyGIJtFeU/7VxtEAJMN2nLNkh2X8/g/yioNqg2qDaoNqg2qDaoNqg2qDaoNqg2qDb0bi0gww6XH+lWrVq1atWrVq1atWrVq1aZQ9uhJEwB/8hKUpSlKUpSlKUpSlKUpSlKUpSlKUpSlKX//2Q==)

> 接口:统一的活动接口`xplan/activity/{type}`
>
> type:`get`
>
> 参数:`type:SEC_KILL`

**返回结果**

```json
{
"result": [
  {
    "id": 133,
    "productName": "日本VAPENo.1儿童电子驱蚊手表 ",
    "tag": "海外直邮",
    "shortDescription": "专为宝宝设计的驱蚊手表，日本原装香港直发 ",
    "saleDesc": "8000元起存免费拿，最高折合预期年化9.84% ",
    "listImage": "http://miz-static.b0.upaiyun.com/miz-image/products/rbqwsh0706_1.jpg",
    "recommendImage": "http://miz-static.b0.upaiyun.com/miz-image/products/rbqwsh0706_1.jpg",
    "sort": 6,
    "invest": "8000元存6个月",
    "defaultPrice": 168,
    "startDate": 1470310122000,
    "showDate": 1470282538000,
    "info": "每日2轮(10点 16点)",
    "totalStock": 997
  }
],
"status": "SUCCESS",
"errorMsg": "成功"
}
```

**处理代码**

```react
//t.state.seckill = 后台返回参数
var t = this;
var seckill = t.state.seckill[0];
var sec = [];
if(seckill){
  var start = new Date(seckill.startDate);
  var txt = '马上抢';
  seckill.totalStock&&new Date()<start&&(txt = `今日${start.getHours()}:${start.getMinutes()}开抢`);
  !seckill.totalStock&&(txt = '已售完');
  sec.push(
    <div key='mysec' onClick={t.toGood}>
      <p><em>秒杀专场</em>{seckill.info}</p>
      <div>
        <img onError={t.onError} ref='secimg' src={seckill.listImage}/>
        <dl>
          <dt>{seckill.productName}</dt>
          <dd className='in'>{seckill.invest}</dd>
          <dd className='price'>官方价：<span>￥{seckill.defaultPrice}</span></dd>
          <dd><a className={seckill.sort?'on':''} href='javascript:;'>{txt}</a></dd>
        </dl>
      </div>
    </div>)
}
```

### 订单详情返回APP(`#/order_detl/{id}`)

> 在需要执行点击返回APP的页面，APP打开URL会传`type=1`

```react
//OrderDetl.js
__event__.setHeader.dispatch({
	title: '订单详情',
	rightBtn: false,
	backBtn:location.href.indexOf('type=1')!==-1?'app':true
});
```

> 点击返回，若传入的`backBtn===’app'`,则执行`location.href = 'mzlicai://close';`

## 2.2.0（160809上线）

### 左右滚动

> 引入swiper3



> 暂时是写死的数据

```html
<section className = 'swiper-container swipergrid'>
  <div className="swiper-wrapper">
    <div className="swiper-slide">
      <img src='http://miz-static.b0.upaiyun.com/miz-image/products/lmgb0726_0.jpg'  />
      <div>
        <span>纯银邮票银镀金钞</span>
        <label>10000元存12个月</label>
        <em>￥705</em>
      </div>
    </div>
    <div className="swiper-slide">
      <img src='http://miz-static.b0.upaiyun.com/miz-image/products/lmgb0726_0.jpg'  />
      <div>
        <span>纯银邮票银镀金钞</span>
        <label>10000元存12个月</label>
        <em>￥705</em>
      </div>
    </div>
    <div className="swiper-slide">
      <img src='http://miz-static.b0.upaiyun.com/miz-image/products/lmgb0726_0.jpg'  />
      <div>
        <span>纯银邮票银镀金钞</span>
        <label>10000元存12个月</label>
        <em>￥705</em>
      </div>
    </div>
    <div className="swiper-slide">
      <dl>
        <dt>查看全部</dt>
        <dd>See All</dd>
      </dl>
    </div>
  </div>

</section>
```

> swiper

```javascript
new Swiper('.swipergrid',{
  direction : 'horizontal',
  slidesPerView : 'auto',
  slidesOffsetAfter:30,
  updateOnImagesReady : true
});
```

### 首页数据请求更新

> 接口`xplan/index/detail_v2`
>
> type:`get`



> 和之前的接口相比，不再传输页数，直接返回所有内容+前段lazyload



> 收到请求

```react
//data = return的数据
t.data.subjects = data.subjects;
t.data.products = data.products;
```

> lazyload使用`anima-lazyload`
>
> `img`不写`url`,地址写到`data-url`
>
> 最后执行`lazyload('img');`

## 2.2.1

### 获取最新的一条信息（#/order_detl/{xOrderId}）

> 接口：`/xplan/logistics/last/{xOrderId}`
>
> type:`get`

```json
{"lastTrace":
	{
		"acceptTime":"2016-08-22 23:07:00",
		"acceptStation":"到达  杭州速递邮件处理中心 处理中心，杭州市",
		"remark":"",
		"status":"PAY_DELIVER"
	},
	"status":"SUCCESS",
	"errorMsg":"成功"
}
```
### 获取所有的物流信息（#/logistics/{xOrderId}）

> 接口:`/xplan/logistics/{xOrderId}`
>
> type:`get`

```json
{
  	"defaultTrace"：{//默认消息
  		remark: null, 
  		acceptStation: "已通知快递员收件",
  		acceptTime: "2016-08-24 15:37:39"
	},
	"logistics":
	{"id":794,
	"mongoId":null,
	"createDate":1471921369000,
	"key":null,
	"modifyDate":1471934751000,
	"xOrderNo":"1608231743000811",
	"shipDate":"2016-08-23",
	"shipNo":"9715444175012",
	"shipExp":"EMS",
	"shipInfo":"[{\"acceptStation\":\"到达  杭州速递邮件处理中心 处理中心，杭州市\",\"acceptTime\":\"2016-08-22 23:07:00\",\"remark\":\"\"},{\"acceptStation\":\"离开金华市 发往杭州市，金华市\",\"acceptTime\":\"2016-08-22 19:16:00\",\"remark\":\"\"},{\"acceptStation\":\"义乌市北苑速递揽投站已收件（揽投员姓名：何占民替,联系电话:13566798477），金华市\",\"acceptTime\":\"2016-08-21 14:39:57\",\"remark\":\"\"}]",
	"status":"PAY_DELIVER",
	"productImage":"http://miz-static.b0.upaiyun.com/miz-image/products/beats0714_0.jpg",
	"statusVal":"在途中",
	"shipExpName":"EMS"
	},
"status":"SUCCESS",
"errorMsg":"成功"
}
```
###订单列表页的`查看物流`（#/order）
> 接口：`/xplan/norder`
>
> type:`get`

返回信息中的`logisticsStatus`为是否有物流信息，有返回`true`