## Mizlicai xplan

### 项目背景

##### xplan理财计划+商城。

### 技术方案

* 规范：
	* ES6
	* CommonJS
* 基础库：
	* Reactjs
	* Reflux
	* ReactRouterComponent
	* Zepto
	* Q
	* Lodash
* 样式：
	* Stylus
* 调试及构建：
	* JSX
	* Sourcemap
	* browserSync
	* Webpack
	* Gulp

### 测试demo

* [demo](index.html)


##2.0 改版
>1.0首页，商品页面,爆款去掉，新的商品页
>秒杀

##2.1
>商品滚动